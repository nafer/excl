program save_recoil_spectrum
  use target_type, only : num_nuc
  use SomeData, only : maxdets
  use parameters, only : read_start, get_sig0_p, set_sig0_p
  use pass_to_conv_integrand, only : mx, detnum, select_all_nuclei, select_only_nucleus
  use utils, only : askd, cprint
  implicit none

  external convolved_spectrum, get_integrated_spectrum, em
  real*8 convolved_spectrum, get_integrated_spectrum, em, mfrom, mto, total

  REAL*8 :: E,  md,zz, bin_width=0.01, Ermax, threshold=0.1d0, zstep, result(4),&
       Efrom, Eto
  integer :: i, k, n, maxbins, steps

  character*1 :: sptype='D', nucleus_resolved='Y'
  character*80 start_file_name, cut_efficiency_file, outfile


  if(iargc()<1) then
     call usage()
     stop
  endif
 
  call getarg(1,start_file_name)
  if(start_file_name.eq."-h") then
     call usage()
     stop
  endif
  
  if(read_start(start_file_name, .false.).eq.0) then
     print *,"Error reading start file "//trim(start_file_name)
     stop
  endif
  if(iargc().gt.1) then
     call getarg(2,cut_efficiency_file)
     call read_cut_file(cut_efficiency_file, 1)
  else
     call read_cut_file("cut_file_does_not_exist__",1)
  endif

  zz=get_sig0_p()
  call askd("Baseline resolution of phonon channel: ", zz)
  call set_sig0_p(zz)
  md=1.


  if(num_nuc().gt.1) then
     call aska("(D)ifferential, (C)ounts above threshold,&
          & (F)raction, (R)ate vs Mx: ",sptype)
  else
     call aska("(D)ifferential, (C)ounts above threshold, (R)ate vs Mx: ",&
          sptype)
  endif
  Ermax=0.
  do i=1,num_nuc()
     Ermax=max(Ermax,em(i,md))
  end do
  select case (sptype)

  case ('D')
     call askd("Wimp mass [GeV]: ",md)
     Ermax=Ermax + 5.*get_sig0_p() 
     call askd("Calculate recoil spectrum up to Emax: ",Ermax)

     bin_width=.001
     call askd("bin width [keV]:",bin_width)
     maxbins=int(Ermax/bin_width)+1
     if(num_nuc().gt.1) then
        call aska("Calculate nucleus resolved curves: ",nucleus_resolved)
     else
        nucleus_resolved="N"
     endif
     print *,"Will calculate energy spectrum convolved with resolution function"
     outfile="dnde.xy"
     open(unit=2,file=outfile)
     write(2,"(a,f8.2,a,f8.3)") "#WIMP mass: ", md,", GeV,  resolution: ",get_sig0_p()
     write(2,*) "#Energy [keV]"
     write(2,*) "#counts/keV/kg-day/10\S-40\N cm\S2\N"
     if(nucleus_resolved.eq."Y") write(2,"('#<',i1,'> ',$)") num_nuc()+1
     write(2,*) maxbins," 1, 0, 0.0"
     detnum=1   ! set in module pass_to_conv_integrand
     mx=md      ! set in module pass_to_conv_integrand
     zz=0
     do i=1,maxbins
        E=(i-0.5)*bin_width
        call select_all_nuclei()
        n=1
        result(n)=convolved_spectrum(E)
        if(nucleus_resolved.eq."Y") then
           do k=1, num_nuc()
              call select_only_nucleus(k)
              n=n+1
              result(n)=convolved_spectrum(E)
           end do
        endif
        write(2,"(5(g15.5,1x))") E, result(1:n)*1.e-4
     end do
     close(unit=2)
     print *,"Energy spectrum convolved with resolution written to "//trim(outfile)

  case('C')   ! integral from threshold to infinity as function of threshold
     !###############################################################################
     call askd("Wimp mass [GeV]: ",md)
     Ermax=Ermax+5.*get_sig0_p()

     bin_width=.001
     call askd("bin width [keV]:",bin_width)
     maxbins=int(Ermax/bin_width)+1
     
     if(num_nuc().gt.1) then
        call aska("Calculate nucleus resolved curves: ",nucleus_resolved)
     else
        nucleus_resolved="N"
     endif
     print *," Will plot integral of expected dN/dE from threshold to",&
          " infinity as function of threshold"

     threshold=0.1
     call askd(" Lowest threshold energy [keV]: ",threshold)
     call askd(" Caculate up to threshold [keV]: ",Ermax)
     outfile="counts_above_thresold.xy"
     open(unit=2,file=outfile)
     write(2,"(a,f8.2,a,f8.3)") "#Expected counts above threshold,  Mx=", md, ", sigma=",get_sig0_p()
     write(2,*) "#Threshold [keV]"
     write(2,*) "#counts/kg-day/10\S-40\N cm\S2\N"
     if(nucleus_resolved .eq. 'Y') then
        if(nucleus_resolved.eq."Y") write(2,"('#<',i1,'> ',$)") num_nuc()+1
     endif
     write(2,*) maxbins-1, 1, 0, 0.0

     result(:)=0.
     Efrom=Ermax
     do i=1,maxbins-1
        Eto=Efrom
        Efrom=Eto-bin_width
        call select_all_nuclei()
        n=1
        result(n)=result(n)+get_integrated_spectrum(md, Efrom, Eto, 1)
        if(nucleus_resolved.eq.'Y') then
           do k=1,num_nuc()
              call select_only_nucleus(k)
              n=n+1
              result(n)=result(n)+get_integrated_spectrum(md, Efrom, Eto, 1)
           end do
        endif
        write(2,1000) Efrom,result(1:n)*1.e-4
1000    format(5(g15.5,1x))
     end do
     close(unit=2)
     print *,"Expected counts above threshold written to file "//trim(outfile)

  case('F','R')
     !#############################################################################
     Ermax=Ermax+10.*get_sig0_p()
     steps=200
     threshold=.1
     mfrom=0.1
     mto=10.
     call askd("From WIMP mass [GeV]:",mfrom)
     call askd("To WIMP mass [GeV]:", mto)
     call aski(" Number of steps: ",steps)
     call askd(" Threshold [keV]: ",threshold)


     if(sptype.eq.'F') then
        outfile="composition_vs_wimp_mass.xy"
        open(unit=2,file=outfile)
        write(2,*) "#Composition vs. WIMP mass, threshold=",threshold," keV"
        print *,'Calculating composition vs. WIMP mass for given threshold'
     else
        outfile="counts_vs_mx.xy"
        open(unit=2,file=outfile)
        write(2,*) "counts above thresold=",threshold," keV"
        print *,'Calculating expected counts above spectrum'
     endif
     
     write(2,*) "#WIMP mass [GeV]"
     if(sptype.eq.'F') then
        write(2,*) "#fraction contributed by nucleus"
        write(2,"('#<',i1,'> ',$)") num_nuc()    
     else
        write(2,*) "#counts/kg-day/10\S-40\N cm\S2\N"
        write(2,"('#<',i1,'> ',$)") num_nuc()+1   
     endif


     write(2,*) steps, ", 1, 0, 0."

     zstep=exp(log(mto/mfrom)/(steps-1))
     md=mfrom
     do i=1,steps
        call select_all_nuclei()
        total=get_integrated_spectrum(md,threshold, Ermax, 1)
        do k=1,num_nuc()
           call select_only_nucleus(k)
           result(k)=get_integrated_spectrum(md,threshold,Ermax,1)
        end do

        if(sptype.eq.'R') then
           write(2,"(5(g15.4,1x))") md, total*1.e-4, result(1:num_nuc())*1.e-4         
        else
           write(2,"(4(g12.4,1x))") md, result(1:num_nuc())/total
        endif

        md=md*zstep
     end do
     close(2)
     print  *, "Results written to file "//trim(outfile)

  case default
     !###############################################################################
     print *,"invalid option"
     stop
  end select

  call system("plotxy "//trim(outfile))
contains
  subroutine usage()
    print *,"**********************************************************************"
    call cprint("Usage is: #save_recoil_spectrum start_file [cut_efficiency_file]#\n")
    print *,"When cut_efficiency_file is missing on command line, energy independent"
    print *,"efficiencies of 1 are assumed"
    print *,"**********************************************************************"
  end subroutine usage
end program save_recoil_spectrum
  

subroutine read_cut_file(fname, num)
  use eff_tables
  implicit none
  character*(*) fname
  integer num
  character*80 temp_str
  integer :: ios, i, k
  numdets=num
  k=num
  open(unit=1,file=fname,status="old",iostat=ios)

  if(ios.ne.0) then
     print *,"Error opening file with cut efficiencies "//trim(fname)
     print *,"Will assume all cut efficiencies are 1"
     cut_bins(k)=0
  else
     read(1,"(a)",err=990) ! skip plotxy header
     read(1,"(a)",err=990) ! skip plotxy header
     read(1,"(a)",err=990) ! skip plotxy header
     read(1,"(a)",err=990) temp_str
     read(temp_str(index(temp_str,"#")+1:),*,err=990) cut_bins(k)
     if(cut_bins(k).gt.max_cut_bins) then
        print *,"Number of elements in file "//trim(fname)//&
             " exceeds limit"
        print *,"Increase size of max_cut_bins at least to ",cut_bins(k)
        stop
     endif
     do i=1,cut_bins(k)
        read(1,*,err=990) ecut(i,k), cut_eff(i,k)
     end do
     read(1,"(a)",iostat=ios) temp_str
     if(index(temp_str,"+").gt.0) then
        read(1,*,err=990) remove_bins(k)
        print *,"Number of energy ranges to remove: ",remove_bins(k)
        if(remove_bins(k).gt.max_remove_bins) then
           print *,"Too many gamma lines to remove"
           print *,"Increase max_remove_bins and remake excl"
        endif
        do i=1,remove_bins(k)
           read(1,*,err=990) Eremove_from(i,k), Eremove_to(i,k)
        end do
     endif
     close(unit=1)
     call sort2(cut_bins(k),ecut(1,k),cut_eff(1,k))
  endif
  return
990 print *,"Error occurred while reading cut efficiencies from "//trim(fname)
  close(unit=1)
  stop
end subroutine read_cut_file
