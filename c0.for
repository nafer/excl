      Real Function C0(x,mu)
      Real x,mu
      Real*8 A1,B,C,C00,x1,mu1,Delta
      If(mu.lt.0.) Then
         If(x.lt.1.) Then
            C0=0.
         Else
            C0=1.
         EndIf
      ElseIf(x.gt.mu) Then
         C0=1.
      ElseIf( (x.lt.0.03*mu.and.mu.lt.60.) .or.
     x   (x.lt.1.8.and.mu.ge.60.) .or.
     x   (x.lt.4. .and.mu.gt.700.) ) Then
         C0=0.
      Else
       x1=x
       mu1=mu
       I=mu1/x1
       A1=exp(-x1)
       C00=1.D0
       C=1.D0
       If(I.ge.1) Then
          C00=1.D0 - (1.D0+mu1-x1)*A1
       EndIf
       If(I.gt.1) Then
C         If(I.gt.30) I=30
         C=1.D0
         Do K=2,I
            B=dfloat(K)*x1 - mu1
            C=C/dFloat(K)
C            C00 = C00 + ((A1*B)**K)*C*(1.D0 - dFloat(K)/B)
            Delta = (A1**K) * (B**(K-1)) * C * (B - DFloat(K))
            C00 = C00 + Delta
            If(Delta.lt.1.E-10) Go to 10
         EndDo
       EndIf
 10    C0=C00
      EndIf
      Return
      End
      
