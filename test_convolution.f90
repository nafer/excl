!**********************************************************************
!                        FUNCTION220 gauss
!**********************************************************************
real*8 function gauss(x,sig)
  implicit none
  real*8, parameter :: sqtwopi=sqrt(3.141592653589793238_8*2.0d0)
  real*8 x, sig, z
  z=x/sig
  gauss=exp(-z*z*0.5d0)/(sqtwopi*sig)
  return
end function gauss

program test_convolution
!  USE nrtype; USE nrutil
!  USE nr

  implicit none
  real*8, parameter :: Emax=100.,dE=0.001,Edec=0.2, sigma=0.025
  integer, parameter :: nmax=2**17
  real*8, dimension(nmax) :: y, f, r, h
  real*8 :: ar, ay, z
  integer i, k 
  external gauss
  real*8 gauss

  do i=1,nmax
     y(i)=exp(-(i-1)*dE/Edec)
  end do
  z=0.5*sum(y)*dE
  y=y/z


  r(1)=gauss(0.d0, sigma)
  do i=1,nmax/2-1
     r(i+1)=gauss(i*dE, sigma)
     r(nmax-i+1)=r(i+1)
  end do

  ay=sum(y)*dE
  ar=sum(r)*dE
  print *,'nmax=',nmax
  print '(3(a,g15.7))','ay=',ay,', ar=',ar, ', ar*ay=',ar*ay


  call realft(y,nmax,+1)
  call realft(r,nmax,+1)
  f=y
  h=r
  f(1)=f(1)*h(1) ! real valued first component
  f(2)=f(2)*h(2) ! real valued last component
  do i=3,nmax-1,2
     k=i+1
     f(i)=f(i)*h(i)-f(k)*h(k)
     f(k)=f(i)*h(k)+f(k)*h(i)
  end do
  call realft(f,nmax,-1) ! back transformation
  
  print '(a,g15.5)','area after convolution',sum(f)*de*dE*2.d0/nmax

  open(unit=1,file='outfile')
  write(1,*) 'Test of convolution with realft'
  write(1,*) 'Energy [keV]'
  write(1,*) 'Function'
  write(1,*) '<2>',nmax, 1, 0, 0.

  do i=1,nmax
     write(1,'(4g15.6)') (i-1)*dE, exp(-(i-1)*dE/Edec)/z, f(i)*dE*2.d0/nmax
  end do
  close(unit=1)
end program test_convolution
