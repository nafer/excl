module yellin_links
  integer, private, parameter :: n=7
  character(len=32) :: yeldir='/opt/Programs/excl/Yellin/',&
       fn(n)=(/&
       'CLtableNew.in ',&
       'CMaxf.in      ',&
       'CMaxfLow.in   ',&
       'CMaxflowNew.in',&
       'ymintable.in  ',&
       'y_vs_CLf.in   ',&
       'CLtable.txt   '/)
contains

  !**********************************************************************
  !                           makeYellinLinks
  !**********************************************************************
  subroutine make_yellin_links()
    implicit none
    integer :: i, status
    logical ex
    do i=1,n
       inquire(file=fn(i),exist=ex)
       if(.not.ex) then
          call symlnk(trim(yeldir)//trim(fn(i)), trim(fn(i)), status)
          if(status.ne.0) then
             print *,"Error creating link to yellin files"
          endif
       endif
    end do
  end subroutine make_yellin_links

  subroutine remove_yellin_links()
    implicit none
    integer i,status
    do i=1,n
       call unlink(fn(i),status)
       if(status.ne.0) print *,"Error deleting link to yellin files"
    end do
  end subroutine remove_yellin_links
end module yellin_links
