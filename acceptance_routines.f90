module Qf_Raimund
  real*8, parameter :: epsilon=0.88
  real*8, parameter :: ACa=1.065d0, BCa=0.05949d0, CCa=0.1887d0, DCa=801.3d0
  real*8, parameter :: AW=1.96E-2
  real*8, parameter :: Ao=1.065d0,  Bo=0.07908d0, Co= 0.7088d0, Do= 567.1d0
end module Qf_Raimund

!*************************************************************
!                  module parameters
!*************************************************************
module parameters
  implicit none
  integer, parameter :: &
       npmax=50,&
       no2=3,&            ! parameters for sigma
       npc_peak=0,&       ! number of common parameters for all peaks
       np_peak=3,&        ! number of parameters per gamma peak
       npc_beta=0,&       ! number of common parameters for all beta spectra
       np_beta=4          ! number of parameters per beta spectrum


  real*8 :: param(npmax)
  real*8, private :: sig0_p, sig1_p
  integer, allocatable :: np_first(:)
  integer :: &
       ip(npmax),&
       max_type,&      ! highest value of suported type
       np, &           ! number of fit parameters (fit+fix)
       no1,&           ! number of parameters used for center of gamma band
       nonr,&          ! number of recoil bands in fit
       nogamma,&       ! number of peaks in gamma background
       nobeta,&        ! number of beta spectra (3 parameters per spectrum)
       
       np_flat,&       ! number of fit parameters for flat gamma background + 
       np_res,&        ! number of fit parameters for phonon resolution
       
       npc_nr=0,&      ! number of common parameters for all recoil     
       np_nr=5         ! number of parameters for recoil nucleus
       
       logical :: interactive, sqtype
  character :: pname(npmax)*10
contains
  !**********************************************************************
  !                  initialize
  !**********************************************************************
  subroutine initialize()
    implicit none
    nonr=0
    nogamma=0
    nobeta=0
    np_nr=5
    npc_nr=0
  end subroutine initialize
  
  !*************************************************
  !                  read_start
  !*************************************************
  integer function read_start(filename, quiet)
    use target_type, only : set_target_type, use_light, &
         max_target_type, get_target_name, get_number_of_bands,&
         get_upper_band, get_lower_band, set_no_light
    use yield_range, only : set_upper_acceptance_band,&
         set_lower_acceptance_band
    use utils, only : ceprint, cenprint
    implicit none
    character*(*), intent(in) :: filename
    logical, intent(in) :: quiet
    integer i, k, ios, n, type
    real*8 :: sig0_phonon, sig1_phonon
    character  program_type*10, temp_str*80
    logical S0_includes_sigp
    read_start=0
    call initialize()
    open(unit=1,file=filename,status='old',iostat=ios)
    if(ios.ne.0) then
       call ceprint("#Error opening file "//trim(filename)//"#")
       stop
    endif

    program_type=""

    read (1,'(a)',iostat=ios) temp_str
    if(ios.ne.0) then
       call ceprint("#Error reading first line of start file "//&
            trim(filename)//"#")
       stop
    endif
    !******************************************************************
    !For non scintillating targets Start file just contains two lines
    ! 1. target: type 
    ! 2. resolution of phonon channel
    !The key string target: triggers limit calculation for nonscintillating
    !targer.
    !******************************************************************
    k=index(temp_str, 'target:')
    if(k.gt.0) then
       read(temp_str(k+8:), *, iostat=ios) type
       if(type.lt.1 .or. type.gt.max_target_type) then
          call ceprint("#Error in routine read_start:\n&
               & Target number is out of range#")
          stop
       endif
       call set_target_type(type)
       if(use_light()) then
          call ceprint("\n&
               & #Error in start file:\n&
               & File format triggering limit calculation for non\n&
               & scintillating target used for scintillating target#")
           call set_no_light()
!          stop
       endif
       read(1,*,iostat=ios) sig0_phonon, sig1_phonon
       call set_sig0_p(sig0_phonon)
       call set_sig1_p(sig1_phonon)
       close(unit=1)
       read_start=1
       np=0
       if(.not.quiet) then
          print *,'Target: ',trim(get_target_name()),', analysis type: 1-d'
       endif
       return
    endif

    ! What follows is only for scintillating targets
    if(index(temp_str,'yield_fit').gt.0) then
       S0_includes_sigp=.false. 
    else
       S0_includes_sigp=.true. 
       call ceprint("#\n&
            & String 'yield_fit' not found in first line of start file\n&
            & Will assume that SO includes phonon resolution#")
    endif


    sqtype=.true.
    if(index(temp_str,"lin").gt.0) then
       sqtype=.false.
       call ceprint("#Error in routine read_start:\n&
            & parameterization of sigma instead of sigma**2 not&
            & supported any more#")
       stop
    endif

    read (temp_str,*,iostat=ios) np, no1, np_flat, nogamma, nobeta, nonr
    if(ios.ne.0) then
       call ceprint("#\n&
            & The first line of the start file contains less than 6 integers:\n&
            &  1: number of fit parameters (fixed+free)\n&
            &  2: number of parameters for center of gamma band\n&
            &  3: number of parameters for flat part of gamma spectrum\n&
            &  4: number of gamma peaks\n&
            &  5: number of beta spectra\n&
            &  6: number of nuclear recoil bands or -target_type\n&
            & When target type is given in integer nr. 6, the number\n&
            & of recoil bands is taken from module target_type\n#")  
       stop
    endif
    if(np.gt.npmax) then
       call cenprint("#Number of fit parameters exceeds limint of# ")
       print '(i3)',npmax
       call ceprint("#Increase npmax in module parameters accordingly#")
       call ceprint("#and rebuild excl#")
       stop
    endif
    if(nonr.gt.0) then
       call ceprint("#\n&
            & The integer number 6 in start file should be the negative\n&
            & value of the target type to use for limit calculation#")
       stop
    endif
    if(-nonr.gt.max_target_type) then
       call ceprint("#\n&
            & The integer number 6 in start file should be the negative\n&
            & value of the target type to use for limit calculation.\n&
            & The target type is above the upper limit of supported targets#")
       stop
    endif


    call set_target_type(-nonr)
    nonr=get_number_of_bands()
    call set_upper_acceptance_band(get_upper_band())
    call set_lower_acceptance_band(get_lower_band())
    npc_nr=2  ! (Mx, sigma)
    np_nr=1   ! only const Qf for each type of recoil nucleus


    if(.not.quiet) then
       print *,'Target: ',trim(get_target_name()),', analysis type: 2-d'
    endif
    do i=1,np
       read(1,*,iostat=ios) ip(i),pname(i), param(i)
       if(ios.ne.0) then
          call cenprint("#Error reading parameter nr. #")
          print *,i
          goto 100
       endif
    end do
    sig0_phonon=-1.d0
    sig1_phonon=0.d0
    read(1,*,iostat=ios) sig0_phonon, sig1_phonon
    if(ios.ne.0 .and. sig0_phonon.lt.0.d0) then
       call ceprint("#Phonon resolution missing at end of start file#")
       stop
    endif
    close(1)

    call set_sig0_p(sig0_phonon)
    call set_sig1_p(sig1_phonon)
    if(S0_includes_sigp) param(no1+1)=param(no1+1) - get_sig0_p()**2


    max_type=nonr+nogamma+nobeta         ! types following continuous gammas
    if(allocated(np_first)) deallocate(np_first)
    allocate(np_first(0:max_type), stat=ios)
    if(ios.ne.0) then
       call ceprint("#Error allocating memory for np_first in read_start#")
       stop
    endif

    np_res=0
    !allow linear energy dependence of phonon resolution when peaks
    !or beta's are fitted
    if(nogamma.gt.0 .or. nobeta.gt.0) np_res=1

    ! store first parameter number of type labeled contributions in np_first 
    type=0
    n=no1 + 6 + 1 
    np_first(type)=n ! points to constant part of flat parametrized gamma spectrum
    n=n + np_flat + np_res                     

    ! recoil bands 
    do i=1,nonr                     
       type=i
       !points to const QF for wimp fit, else to scale factor before exponential
       np_first(type) = n + npc_nr + np_nr*(i-1)    
    end do
    n = n + npc_nr + np_nr*nonr

    ! copied from yield_fit. Not needed from here in excl
    ! gamma peaks

    do i=1,nogamma    
       type=i+nonr
       np_first(type) = n + npc_peak + (i-1)*np_peak 
    end do
    n = n + npc_peak + np_peak*nogamma

    ! beta spectra
    do i=1,nobeta  
       type=i+nonr+nogamma
       np_first(type)=n + npc_beta+np_beta*(i-1)
    end do

    read_start=1
    return
100 call ceprint("#Error reading parameters from file "//trim(filename)//"#")
    stop
  end function read_start


  real*8 function get_sig0_p()
    get_sig0_p=sig0_p
    return
  end function get_sig0_p

  real*8 function get_sig1_p()
    get_sig1_p=sig1_p
    return
  end function get_sig1_p

  subroutine set_sig0_p(sig)
    implicit none
    real*8 sig
    sig0_p=sig
    return
  end subroutine set_sig0_p

  subroutine set_sig1_p(sig)
    implicit none
    real*8 sig
    sig1_p=sig
    return
  end subroutine set_sig1_p
  !********************************************************
  !              get_sigma
  !********************************************************
  subroutine get_sigma(E,El,sig,type)
    ! calculate sigma sig of gaussion for event
    ! type (0=gamma, 1=Ca, 2=W, 3=O) of given light energy El
    implicit none

    real*8, intent(in) ::  E, El 
    integer, intent(in) :: type
    real*8, intent(out) :: sig
    real*8 :: z

    integer i
    sig=0. 
    z=1.
    do i=no1+1,no1+no2
       sig=sig+param(i)*z
       z=z*El
    end do
    z=sig0_p*get_slope(E,type)
    sig=sqrt(sig + z*z)
  end subroutine get_sigma

  !***********************************************************
  !              get_center
  !***********************************************************
  subroutine get_center(E,El, type)
    ! Light energy of center of band as function of phonon energy
    ! input:
    ! E    Energy of phonon channel
    ! El   Center of  band (Energy of light channel)
    implicit none
    real*8, intent(in) :: E
    integer, intent(in) :: type  
    real*8, intent(out) :: El

    real*8 z
    integer i
    z=E
    El=0.
    do i=3,no1
       El=El+param(i)*z
       z=z*E
    end do
    El=El/QF(E,type)

    return
  end subroutine get_center


  !**********************************************************************
  !                      get_slope
  !**********************************************************************
  real*8 function get_slope(E, type)
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ! get slope of center of band of type in Elight/Ephonon plane
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    implicit none
    real*8, intent(in) :: E
    integer, intent(in) :: type
    real*8 z, z1, z2, Q
    integer i

    z=E
    z1=param(3)*E
    z2=param(3)
    do i=4,no1
       z2=z2+(i-2)*param(i)*z
       z=z*E
       z1=z1+param(i)*z
    end do

    Q=QF(E,type)
    get_slope=(z2 -z1*dQF_dE(E,type)/Q)/Q

!!$    print *,'E,type,slope:',sngl(E),type,sngl(get_slope)
    return
  end function get_slope

  !**********************************************************************
  !                         get_probab
  !**********************************************************************
  real*8 function get_probab(E,yield, type)
    !----------------------------------------------------------------------
    ! get the probability to find any yield lower than the given yield for 
    ! ponon energy
    !----------------------------------------------------------------------

    implicit none
    real*8, intent(in) :: E, yield
    integer, intent(in) :: type

    real*8  El, sig, y
    real*8, parameter :: sqrt2=sqrt(2.d0)
    call get_center(E, El, type)
    call get_sigma(E,El,sig,type)  ! resolution of LD at quenched energy
    y=(yield*E-El)/(sig*sqrt2)
    get_probab=(1.+erf(y))*0.5d0
  end function get_probab


  !**********************************************************************
  !                           get_yield_limit
  !**********************************************************************
  real*8 function get_yield_limit(energy, type, fraction)
    implicit none
    real*8, intent(in) :: energy,fraction
    integer, intent(in) :: type   !0=gamma band, 1=Ca, 2=W, 3=O
    real*8 ::  x1, x2, tol=1.e-8, El

    real*8 mu, sig, cl
    common /pass_to_fnull/ mu, sig, cl

    external fnull,zbrent
    real*8 fnull,zbrent

    cl=fraction
    call get_center(energy,El,type)
    call get_sigma(energy,El,sig,type)  ! sigma in El-Ep plane
    mu=El
    x1=el-7.*sig                 ! very generous bracketing of zero
    x2=el+6.*sig                 ! very generous bracketing of zero
    get_yield_limit=zbrent(fnull,x1,x2,tol)/energy

    return
  end function get_yield_limit


  !**********************************************************************
  !                     QF
  !**********************************************************************
  real*8 function QF(E,type)
    use Qf_Raimund
    use target_type, only : get_target_type, use_light,&
         get_number_of_bands, get_target_name, get_band_of_nucleus
    use utils, only : ceprint

    implicit none
    real*8, intent(in) :: E
    integer, intent(in) :: type
    integer band

    if(type.eq.0) then  ! non proportionality of e/gamma band
       QF=1.d0/(1.d0 - param(2)*exp(-E/param(1))) 
       return
    endif

    if(get_target_type() .eq. 1) then  ! CaWO4
       !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       !Quenching factors taken from Raimund's paper,
       !epsilon mean for run32 detectors and
       ! TUM40 and Lise
       !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

       select case(type)
       case(1) ! Ca
          QF=ACa/(BCa*epsilon*(1.d0 + Cca*exp(-E/DCa)))
       case(2)   ! W
          QF=1./(epsilon*AW  )
       case(3) ! O
          QF= Ao/(Bo*epsilon*(1.d0 + Co*exp(-E/ Do)))
       case DEFAULT
          print *,'Error in function QF: illegal type ',type
          print *,'Supported values for type are 0 to 3'
          stop
       end select
       return
    endif
    
    if(.not.use_light()) then
       call ceprint("#Error in subroutine QF:\n&
            & Tried to calculate quenching factor for a\n&
            & non scintillating target#")
       stop
    endif
    band=get_band_of_nucleus(type)
    if(band.eq.0 .or. band.gt.get_number_of_bands()) then
       call ceprint("#Error in subroutine QF:\n&
            & band is outside allowed range#")
       stop
    end if
    QF=param(np_first(band))  ! for recoil bands
  end function QF

  !**********************************************************************
  !                         dQF_dE
  !**********************************************************************
  real*8 function  dQF_dE(E,type)
    use Qf_Raimund
    use target_type, only : get_target_type
    implicit none
    real*8, intent(in) :: E
    integer, intent(in) :: type
    real*8 z,z1
    if(type.eq.0) then
       z=param(2)*exp(-E/param(1))
       z1=1./(1.- z)
       dQF_dE=-(z1*z1*z)/param(1)
       return
    endif
    if(get_target_type() .eq. 1) then  ! CaWO4 
       select case(type)
        case(1) 
          z=CCa*exp(-E/DCa)
          dQF_dE=z*ACa/((BCa*DCa)*(1.d0+z))
       case(2)
          dQF_de=0.d0
       case(3)
          z=Co*exp(-E/Do)
          dQF_dE=z*Ao/((Bo*Do)*(1.d0+z))
       case DEFAULT
          print *,'Error in function dQF_dE: illegal type ',type
          stop
       end select
       return
    endif
    dQf_dE=0.
  end function dQF_dE

end module parameters


real*8 function fnull(x)
  !zero of this function gives light energy for which the probability 
  ! to have less light than that is cl
  implicit none
  real*8 mu, sig
  real*8 cl
  common /pass_to_fnull/ mu, sig,cl
  real*8, parameter :: sqrt2=sqrt(2.)
  real*8 x,y
  y=(x-mu)/sig
  fnull=(1.+erf(y/sqrt2))*0.5-cl
  return
end function fnull

!
