#*********************** Program excl ***************************************#
 Usage is: #excl taskfile [options]#
 Options:
  #-fu <x>#     Fraction of uppermost band to include in
  # #           acceptance region (default=0.5)
  #-fl <x>#     Fraction of lowest band to exclude from
  # #           acceptance region (default=0.005)
  #-mfrom <x>#  calculate limit from this WIMP mass [GeV]
  #-mto <x>#    calculate limit up to this mass [GeV]
  #-steps <n>#  number of steps per mass decade
  #-quiet#      To suppess asking questions
 Space is required between option and corresponding value.
 The -fl and -fu options are ignored when a non scintillating
 target is selected in the start file.
         
  The taskfile gives a number of files needed for each detector.
  For #non scintillating# targets the #start file# has only two lines:
  target: number
  sigma_phonon(E=0), dsigma_phonon/dE
  Here, the keyword 'target:' triggers reading of the number
  specifying the non scintillating target.
  
  7 target materials are presently implemented:
  #1: CaWO4, 2: Al2O3, 3: Ge, 4: Si, 5: LiAlO2, 6: Diamond, 7: Al2O3 no light#
  CaWO4 and Sapphire are implemented as scintillating targets.
  For simplicity Sapphire is also available as non scintillating tartet.
  For scintillating targets you need a start file with fit parameter
  as determined by yield_fit, discribing bands in the energy-light
  plane. CaWO4 uses fixed quenching factors taken from Raimund's
  paper, and a start file from a fit of the gamma band should be
  sufficient.
  For Sapphire and possible future scintillating targets a start
  file in the format for fitting WIMP energy-light data is required.
         
#  Get help on structure of files:#
  #excl -task#     Info on structure of task file
  #excl -start#    Info on structure of start file
  #excl -data#     Info on structure of data file
  #excl -exposure# Info on structure of exposure file
  #excl -cut#      Info on structure of file with cut survival probabilities
#*****************************************************************************#