!**********************************************************************
!                  module astphyparam
!**********************************************************************
module astphyparam
  real*8, parameter ::&
       pi=3.14159265359,&
       sqrt_pi=sqrt(pi),&
       amu=0.931494028,&        ! atomic mass unit in GeV/c**2
       amukg=1.660538782e-27,&  ! atomic mass unit in kg/amu
       ro=0.3,&          	! local density GeV/cm**3
       v0=220.,&         	! velocity parameter km/s
       w=v0*sqrt(1.5),&	        ! w=vrms=v0*sqrt(3./2.)
       c=299792.458,&   	! speed of light in km/s
       vesc=544.,&       	! escape velocity
       ve=v0*1.05,&         	! earth relative velocity (griest)
       zv=vesc/v0,&             ! the z in Donato' Appendix (Ap 9 (1998) 247-260
       eta=ve/v0,&              ! the eta in Appendix of Donato et al
       mp=0.938272013d0         ! proton mass in GeV/c**2
end module astphyparam


!***********************************************************^
!                        em
!***********************************************************
real*8 function em(nuc,mx)
  ! get largest possible recoil energy 
  use astphyparam, only : vesc, ve, amu, c
  use target_type, only : An, num_nuc
  use utils, only : cenprint
  implicit none
  real*8, intent(in) :: mx
  integer, intent(in) :: nuc
  real*8, parameter :: v=vesc+ve
  real*8 A, mn, mred, z
  
  if(nuc.gt.num_nuc()) then
     call cenprint("#Error in subroutine em:\n&
          & Called with a too large number for type of nucleus:#")
     print *,nuc
     print *,"Largest available number for nucleus:",num_nuc()
     stop
  endif
     
  A=An(nuc)
  mn=A*amu      
  mred=(mx*mn)/(mx+mn)
  z=mred*v/c
  em=2.d6*z*z/mn
  return
end function em

real*8 function gauss(x,sig)
  implicit none
  real*8, parameter :: sqtwopi=sqrt(3.141592653589793238_8*2.0d0)
  real*8 x, sig, z
  z=x/sig
  gauss=exp(-z*z*0.5d0)/(sqtwopi*sig)
  return
end function gauss

!**********************************************************************
!               module pass_to_conv_integrand
!**********************************************************************
module pass_to_conv_integrand
  ! helper module for passing some info to convolution integrand
  ! (this violates thread safety)
  real*8 :: &
       Er,&           ! energy [keV] for which to calculate convolution
       mx,&           ! WIMP mass (GeV/c**2)
       fnuc(3)
  integer :: &
       nuc,&          ! type of nucleus Ca, W, O (1,2,3)
       detnum         ! detector number
  
  logical :: nucleus_selected(3)=(/.true.,.true.,.true./)

  contains
    subroutine select_only_nucleus(num)
      implicit none
      integer num
      nucleus_selected(:)=.false.
      nucleus_selected(num)=.true.
    end subroutine select_only_nucleus

    subroutine select_all_nuclei()
      nucleus_selected(:)=.true.
    end subroutine select_all_nuclei

end module pass_to_conv_integrand


!**********************************************************************
!                     dN_dE
!**********************************************************************
real*8 function dN_dE(A, Mx, Erec)
  !*********************************************************************
  !calculate dN/dE (counts per day per keV) at recoil energy Erec for 
  !1 kg of target material of only this nucleus and 1 pb point like
  !WIMP-nucleon cross section
  !input: 
  !   A       atomic number of target nucleus
  !   Mx      WIMP mass in GeV/c**2
  !   Erec    Energy in keV
  !**********************************************************************
  use astphyparam, only : c, amu, eta, ro, sqrt_pi, v0, zv
  implicit none
  real*8, intent(in) :: A, mx, Erec
  Real*8 mn, mred, rx, xmin, fn
  real*8, parameter :: xfact=c*c*1.d-6*1.d5*24.*3600.
  dN_dE=0.d0
  if(Erec.le. 0.d0) return

  mn=A*amu              ! mass of nucleous
  mred=mn*mx/(mn+mx)    ! reduce mass of WIMP nucleon system

  xmin=sqrt(mn*Erec*1.d-6/(2.d0*(v0*mred/c)**2))

  if(xmin.lt.zv-eta) then
     rx=chi(xmin-eta, xmin+eta) - 2.*eta*exp(-zv*zv)
  elseif(xmin.lt.zv+eta ) then
     rx=chi(xmin-eta,zv) - exp(-zv*zv)*(zv+eta-xmin)
  else
     return
  endif

  fn=erf(zv)-2*zv*exp(-zv*zv)/sqrt_pi        ! normalization
  rx=rx/(eta*sqrt_pi*fn*v0)
  dN_dE=xfact*rx*mn*ro/(2*mx*mred*mred)
  return
contains

  real*8 function chi(x,y)
    implicit none
    real*8, intent(in) :: x,y
    chi=0.5d0*sqrt_pi*(erf(y)-erf(x))
    return
  end function chi

end function dN_dE



!**********************************************************************
!                         conv_integrand
!**********************************************************************
real*8 function conv_integrand(E)
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ! integration of this function from -10*sigma to ~10*sigma should give
  ! recoil spetrum of nucleus nuc for wimp mass mx convolved with resolution
  ! function of the phonon channel
  ! The desired recoil energy Er, for which to compute convolution 
  ! mx, nuc, detnum have to be passed via module pass_to_conv_integrand
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  use pass_to_conv_integrand, only : mx, Er, nuc
  use astphyparam, only : amu, amukg, mp, pi
  use target_type, only : An, weight_fraction
  use parameters, only : get_sig0_p, get_sig1_p
  implicit none
  external dN_dE, gauss
  real*8 dN_dE, gauss

  real*8, intent(in) :: E
  real*8, parameter :: mabs=1.d0              ! absorber unit mass[kg]

  real*8 mpr, mnuc, mnr, Nt, sigma, A

  A=An(nuc)                ! Atomic mass of nucleus (nuc from pass_to_conv_integrand)
  mpr=mp*mx/(mp+mx)        ! mx from pass_to_conv_integrand          

  mnuc=A*amu               ! mass of target nucleus [GeV/c**2]
  mnr=(mnuc*mx)/(mnuc+mx)  ! reduced mass wimp nucleus
  Nt=mabs/(A*amukg)


  conv_integrand=1.d-36*dN_dE(A,mx,Er-E)*weight_fraction(nuc)*Nt*&
       (form(Er-E)*(mnr*A/mpr))**2
  sigma=get_sig0_p() + get_sig1_p()*Er 
  if(sigma.gt.0.d0) conv_integrand=conv_integrand*gauss(E,sigma)
  return
contains

  REAL*8 function form(Erec)
    !************************************************************
    !nuclear form factor for coherent scattering 
    !Lewen & Smith parameterization of helm form factor
    !input:
    !  a   : atomic number of target nucleous
    !  er  : recoil energy transferred to target nucleous in kev
    !************************************************************
    implicit none
    real*8, intent(in) :: Erec
    real*8 x, q, r0, c0
    real*8, parameter :: s=0.9,  an=0.52
    real*8, parameter :: piasq=7.d0*pi*pi*an*an/3.d0

    if(Erec.le.0.0) then
       form=1.
       return
    endif

    c0=1.23d0*A**(1./3.) -0.6d0 
    R0=sqrt(c0*c0 + piasq - 5.d0*s*s)
    q=sqrt(2.d0*a*amu*Erec)/197.326960d0
    x=q*r0
    form=3.d0*(sin(x)/x - cos(x))*exp(-0.5d0*(s*q)**2)/(x*x)
    return
  end function form
end function conv_integrand
 
 !**********************************************************************
 !                         convolved_spectrum
 !**********************************************************************
real*8 function convolved_spectrum(E)
  ! This function returns the convolved energy spectrum at energy E
  !
  ! Sums recoil spectra of target nuclei (e.g. Ca,W,O) of detector detnum
  ! at energy E, already convolved with detector specific phonon resolution.
  ! Account for yield range selection of acceptance region (2d case) and
  ! for detectorspecific cut and trigger efficiencies
  ! 

  use pass_to_conv_integrand!, only : Er, sig0, detnum, nuc, fnuc, nucleus_selected
  use yield_range, only : fraction, get_upper_acceptance_band, get_lower_acceptance_band
  use eff_tables, only : ecut, cut_eff, cut_bins, etrig, trig_eff, trig_bins
  use parameters, only : get_sig0_p, get_sig1_p, get_yield_limit, get_probab
  use target_type, only : num_nuc, use_light
  implicit none
  real*8, intent(in) :: E
  real*8, parameter :: epsabs=-1.d0, epsrel=0.5d-9
  integer, parameter :: limit=10000            ! maximum number of function evaluations
  real*8 :: llim, ulim                         ! range of convolution integration
  real*8 abserr                                ! returned estimate of absolute error
  real*8 alist(limit), blist(limit), rlist(limit), elist(limit)
  real*8 sigma, proba, ceff, dceff, teff, dteff, yield_max, yield_min, z0, z1, psum
  integer iord(limit), last
  integer neval                               ! number of function evaluations needed
  integer ier                                  ! 0=success  

  integer m
  integer, save :: j
  integer :: j1=1, j2=1

  external conv_integrand 
  real*8 conv_integrand

  Er=E                    ! set energy Er in module pass_to_conv_integrand
  sigma=get_sig0_p() + get_sig1_p()*Er
  z1=0.d0
  psum=0.
  do nuc=1,num_nuc()              ! set nuc in module pass_to_conv_integrand
     if(nucleus_selected(nuc)) then
        ! convolve recoil spectrum of nucleus with phonon resolution function
        if(sigma.gt.1.d-10) then
           z0=0.d0
           llim=-10.d0*sigma
           ulim=min(Er-tiny(Er),10.d0*sigma)
           call dqagse(conv_integrand,llim, ulim, epsabs, epsrel, limit, z0,&
                abserr,neval, ier, alist, blist, rlist, elist, iord, last)          

           if(ier.gt.2) then
              print *,'Energy: ',sngl(Er)
              print *,'Error returned in calculation of convolution integral:',ier
           endif

        else
           z0=conv_integrand(0.d0)
        endif

        !     print *,'actual number of function calls in convolution: ',neval

        ! account for accepted yield range 

        proba=1.d0
        if(use_light()) then
           if(fraction(1).lt. 0.999999d0) then
              yield_max=get_yield_limit(Er,get_upper_acceptance_band(),fraction(1)) ! Oxygen
              proba=get_probab(Er,yield_max,nuc)
           endif

           if(fraction(2).gt.1.d-6) then
              yield_min=get_yield_limit(Er,get_lower_acceptance_band(),fraction(2)) ! W
              proba=proba-get_probab(Er,yield_min, nuc)
           endif
        endif
        fnuc(nuc)=proba
        psum=psum+proba
        z1=z1+z0*proba
     endif
  enddo

  fnuc(1:num_nuc())=fnuc(1:num_nuc())/psum

  m=2
  ceff=1.d0
  if(cut_bins(detnum).gt.0) then
     ! account for detector specific cut and trigger efficiencies
     call hunt(ecut(1,detnum),cut_bins(detnum), Er,j1)
     if(j1.eq.cut_bins(detnum)) then   ! use last element instead of extrapolation
        ceff=cut_eff(j,detnum)
     else
        j=min(max(j1-(m-1)/2,1),cut_bins(detnum)+1-m)
        call polint(ecut(j,detnum),cut_eff(j,detnum),m, Er, ceff, dceff)
     endif
  endif

  teff=1.d0 
  if(trig_bins(detnum).gt.0) then
     call hunt(etrig(1,detnum),trig_bins(detnum),Er,j2)
     j=min(max(j2-(m-1)/2,1),trig_bins(detnum)+1-m)
     call polint(etrig(j,detnum),trig_eff(j,detnum),m,Er,teff,dteff)
  endif
  convolved_spectrum=z1*ceff*teff

  return
end function convolved_spectrum

 !**********************************************************************
 !                      get_integrated_spectrum
 !**********************************************************************
 real*8 function get_integrated_spectrum(m, Efrom, Eto, detector)
   ! integrate recoil spectrum already convolved with resolution function  over
   ! energy range from Efrom to Eto 
   use pass_to_conv_integrand, only : mx, detnum
   use parameters, only : get_sig0_p
   use target_type, only : num_nuc
   implicit none
   real*8, intent(in) :: m, Efrom, Eto
   integer, intent(in) :: detector
   external convolved_spectrum, em
   integer i
   real*8 em, Eo

   integer, parameter :: limit=10000     ! maximum  number of function evaluations
   real*8, parameter :: epsabs=-1.d0, epsrel=1.d-4

   real*8 abserr                        ! returned estimate of absolute error
   real*8 alist(limit), blist(limit), rlist(limit), elist(limit)
   integer neval                        ! number of function , evaluations needed
   integer ier                                  ! 0=success
   integer iord(limit), last

   mx=m                ! set in module pass_to_conv_integrand
   detnum=detector     ! set detnum in pass_to_conv_integrand

   Eo=0.
   do i=1,num_nuc()
      Eo=max(Eo,em(i,mx))
   end do

   Eo=min(Eto,Eo+10.*get_sig0_p())
   call dqagse(convolved_spectrum,Efrom, Eo, epsabs, epsrel, limit, &
        get_integrated_spectrum,&
        abserr,neval, ier, alist, blist, rlist, elist, iord, last)
      if(ier.ne.0) then
         print *,'Error returned from integration in get_integrated_spectrum:', ier
      endif
   return
 end function get_integrated_spectrum

