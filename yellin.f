c######################################################################
c                  UpperLimNew.f
c######################################################################
      Real Function UpperLim(CL,If,N,FC,muB,FB,Iflag)
C Calls y_vs_CLf, which calls DGAUSN, and ConfLev, which calls GAMDIS,
C both of CERNLIB (libmathlib and libkernlib).  But in Jan 2010 changed
C to use ConfLevNew, which calls ConfLev if necessary.
C
C Suppose you have a set of N events distributed in some 1-d variable
C and want to know the CL confidence level upper limit on the mean of
C the expected number of events.  Assume there's expected distribution
C characterized by some cumulative probability function, on top of
C which is an unknown background.  UpperLim is the optimum interval
C upper limit, taking into account deviation between the observed
C distribution and the predicted one.
C
C CL is the confidence level desired
C If says which minimum fraction of the cumulative probability
C   is allowed for seeking the optimum interval.  If=1,2,3,4,5,6,7
C   corresponds to minimum cumulative probability interval =
C   .00, .01, .02, .05, .10, .20, .50.
C N is the number of events
C FC: Given the foreground distribution whose shape is known, but whose
C    normalization is to have its upper limit total expected number of
C    events determined, FC(0) to FC(N+1), with FC(0)=0, FC(N+1)=1, and with 
C    FC(i) the increasing ordered set of cumulative probabilities for the
C    foreground distribution for event i, i=1 to N.
C muB is the total expected number of events from known background.
C FB is like FC but assuming the distribution shape from known background.
C Iflag is the return code flag.:
C   0: Normal return
C  The flag bits correspond to:
C   1: More than 5 iterations required.  Unusual, but should be ok.
C   2: More than 10 iterations needed, but not obtained.  This may be serious.
C    Other bits in Iflag refer to what happened in the last iteration
C   4: y_vs_CLf returned status 1 at some time (extrapolated from f0=0.01)
C   8: y_vs_CLf returned status 2 at some time (extrapolated from f0=1)
C  16: The optimum interval had status 1.
C  32: The optimum interval had status 2.
C  64: Failure to solve CMax = CMaxbar.
C 128: Couldn't solve CMax=CMaxbar because upperlim wants to be <0.
C 256: More than one solution.  The other two are in Exclude_low, which gives
C      the excluded range below the absolute upper limit.
C If something goes wrong which prevents return of correct results, the
C program prints an error message and stops.
C  There is also a Common/UpperLimCom/EndPoints(2),Exclude_low(2).
C  EndPoints(1) is an integer giving the I of FC(I) at which the optimum
C  interval started and EndPoints(2) is the I at which the optimum interval
C  ended.  When Iflag has bit 256 on, everything above UpperLim is excluded,
C  as is everything between Exclude_low(1) and Exclude_low(2).  But between
C  Exclude_low(2) and UpperLim is allowed.
      Implicit None
      Integer N,If,Iflag,NMax,I,m,Niter,Istat,IflagOpt,MaxF,
     1 N1,If1,NCalls,I1,I2,mmax,EndPoints
      Parameter (NMax=150000)
      Real MeanMax
C The number of iteration, Niter, can be 5 and it only occasionally needs
C more for low mu and with lots of background.  Even then, 5 is enough to
C almost always get almost exactly the same answer.
      Parameter (Niter=10)
      Real CL,FC(0:N+1),y_vs_CLf,CMaxinf,mu,mu0,f(0:NMax),y,y2,x,
     1 CMax,fmin(7)/.00,.01,.02,.05,.1,.2,.5/,eps/.0001/,Cdbg,
     2 fdebug,ydebug,mutmp,R,Topmu,Botmu,CL1,muB,FB(0:N+1),FC1,FB1,muB1,
     3 FUp,fmin1(7),mucand(3),TopNow,BotNow,FUPbot,FUPnow,Exclude_low,
     4 fhigh,fdiff
      Common/Fupcom/f,N1,CL1,If1,MeanMax,NCalls,Istat,FC1(0:NMax),
     1 FB1(0:NMax),muB1,fmin1,Cdbg(0:10),mmax
      Common/UpperLimcom/EndPoints(2),Exclude_low(2)
      Logical debug/.false./
      External FUp
      If(N.ge.NMax) Then
         Write(6,*) N,", the number of events, is above ",NMax-1
         Stop
      EndIf
      Do I=1,3
         mucand(I)=-1.
      EndDo
      MeanMax=54.5
      FC(0)=0.
      FC(N+1)=1.
      muB1=muB
      Do I=1,7
         fmin1(I)=fmin(I)
      EndDo
      If(muB.ne.0.) Then
         FB(0)=0.
         FB(N+1)=1.
         Do I=0,N+1
            FC1(I)=FC(I)
            FB1(I)=FB(I)
         EndDo
      EndIf
C For each m=0 to N find f(m), the maximum over all I from 0 to N-m of
C FC(I+m+1)-FC(I).  Start out with mu0=float(N) and evaluate
C CMax=CMaxinf(CL,If,mu).  For each m, y=y_vs_CLf(CMax,f(m)), and x from
C y=(m-x)/sqrt(x).  Find the smallest value of x/f(m) and call
C it the new mu.  Iterate until the fractional change of mu < eps, at which
C time take UpperLim=mu.
      If(CL.lt.0.8 .or. CL.gt. 0.995) Then
         Write(6,*) CL,
     1    " is out of the permissible confidence level range."
         Stop
      EndIf
      If(If.lt.1 .or. If.gt.7) Then
         Write(6,*) If,
     1    " is out of the permissible value selecting fmin."
         Stop
      EndIf
      Iflag=0
      If(N.eq.0) Then
         UpperLim=log(1./(1.-CL))
         EndPoints(1)=0
         EndPoints(2)=0
         Return
      EndIf
      If(muB.eq.0.) Then
       Do m=0,N
         f(m)=0.
         Do I=0,N-m
            f(m)=Max(f(m),FC(I+m+1)-FC(I))
         EndDo
       EndDo
      EndIf
C For some reason, the quick method of convergence sometimes fails
C with muB>0.
      I1=1
      If(muB.ne.0.) Go to 50
      mu0=Float(N)
      Do I=1,Niter
         If(mu0.lt.MeanMax) GoTo 50
         CMax=CMaxinf(CL,If,mu0)
         mu=1.E10
         Do m=0,N
            If(muB.ne.0.) Then
               f(m)=0.
               Do I2=0,N-m
                  f(m)=Max(f(m),(1.-(muB/mu0))*(FC(I2+m+1)-FC(I2))+
     1                (muB/mu0)*(FB(I2+m+1)-FB(I2)))
               EndDo
            EndIf
            If(f(m).gt.fmin(If)) Then
               y=y_vs_CLf(CMax,f(m),Istat)
               If(Istat.gt.2) Then
                  Write(6,*) "y_vs_CLf returned with status",Istat
                  Go to 50
               EndIf
               Iflag=Or(Iflag,4*Istat)
               y2=y*y
               x=float(m)+ .5*(y2+sqrt(y2*(4.*float(m)+y2)))
               mutmp=x/f(m)
               If(mutmp.lt.mu) Then
                  mu=mutmp
C IflagOpt will have the status of the optimum interval
                  IflagOpt=Istat
                  mmax=m
                  fdebug=f(m)
                  ydebug=y
               EndIf
             EndIf
         EndDo
         If(abs(mu0-mu)/mu .lt. eps .and. I.gt.1) Go to 100
         mu0=max(muB,mu)
         If(mu0.lt.muB) Go to 50
         If(I.eq.5) Then
            Iflag=Iflag+1
            GoTo 50 ! It looks like it won't converge (often with muB>0)
         EndIf
      EndDo
      Iflag=Iflag+2
      Write(6,*) "UpperLim did the maximum number of iterations,",
     1 Niter
 50   Continue
C Come here if it's starting to look like mu<54.5, or if convergence
C fails for mu>54.5.
      IflagOpt=0
      MAXF=500
      N1=N
      CL1=CL
      If1=If
      Topmu=float(N)+4.*sqrt(Float(N))+5.
      Botmu=max(muB,log(1./(1.-CL)))
      If( muB.gt.0. .and. FUp(Botmu,1) .gt.0.) Then
C It looks like UpperLim wants to be negative
         UpperLim=0.
         Iflag=IFlag+192
         Return
      EndIf
      Call RZERO(Botmu,Topmu,mu,R,EPS,MAXF,FUp)
      If(R.lt.0. .or. Istat.gt.4) Iflag=Iflag+64
      If(mu.gt.2.5 .and. mu.lt.Max(12.,15.*CL-1.5)) Then
C For small mu look for multiple solutions.  CL=.8 has its first jump at
C mu=3 and its 6th around 10.6; CL=.9 has its first jump at 3.9 and its
C 6th at 12.01; the table of CMax ends at 12 and covers CL=.8+ to near 1.
         BotNow=Min(2.5,mu-1.5)
         TopNow=BotNow
         FUpbot=FUp(BotNow,1)
         Do I=1,300
C         Do I=1,1000
            TopNow=TopNow+.01
C            TopNow=TopNow+.003
            If(TopNow.gt.12.) Go to 100
            FUpnow=FUp(TopNow,1)
            If(FUpbot*FUpnow.lt.0.) Then
               Call RZERO(BotNow,TopNow,mucand(I1),R,EPS,MAXF,FUp)
               mucand(I1)=mucand(I1)-muB
               BotNow=TopNow
               FUpbot=FUpnow
               I1=I1+1
               If(I1.gt.3) Go to 100
            EndIf
         EndDo
      EndIf
 100  UpperLim=mu-muB
      If(I1.gt.2) Then
         UpperLim=mucand(3)-muB
         Exclude_low(1)=mucand(1)-muB
         Exclude_low(2)=mucand(2)-muB
         Iflag=Or(Iflag,256)
      EndIf
      Iflag=Or(Iflag,16*IflagOpt)
C mmax is now the number of events in the optimum interval.  Find the
C endpoints of the optimum interval.
      fhigh=-1.
      Do I=0,N-mmax
         fdiff=FC(I+mmax+1)-FC(I)
         if(fdiff.gt.fhigh) Then
            fhigh=fdiff
            EndPoints(1)=I
            EndPoints(2)=I+mmax+1
         EndIf
      EndDo
      If(debug) Then
          Write(6,200) N,mmax,mu,fdebug,ydebug,fhigh,EndPoints
 200     Format('N, m, mu, f(m), y_vs_CLf(CMax,f(m)), fhigh, Endpoints',
     1 2I5,F12.2,3F9.5,2I6)
C        Write(6,210) mmax,(FC(I),I=1,N)
C 210     Format('m, FC: ',I4,(10F8.5))
C         Write(6,215) (f(I),Cdbg(I),I=0,N)
C 215     Format('f,C: ',10F8.5)
      EndIf
      Return
      End
      Real Function Fup(x,I)
      Implicit None
      Integer N1,NMax,I,If1,Istat,m,NCalls,I1,mmax
      Parameter (NMax=150000)
C      Real f(0:NMax),x,C,y,CMax,CL1,MeanMax,Cinf,CMaxinf,ConfLev,
      Real f(0:NMax),x,C,y,CMax,CL1,MeanMax,Cinf,CMaxinf,ConfLevNew,
     1 CMxinf,FC1,FB1,muB1,fmin1(7),Cdbg
      Common/Fupcom/f,N1,CL1,If1,MeanMax,NCalls,Istat,FC1(0:NMax),
     1 FB1(0:NMax),muB1,fmin1,Cdbg(0:10),mmax
      Logical Debug2/.false./
      Real FUpsave
      save FUpsave
      If(I.eq.1) NCalls=0
      CMax=0.
      CMxinf=CMaxinf(CL1,If1,x)
      Do m=0,N1
         If(muB1.ne.0.) Then
            f(m)=0.
            Do I1=0,N1-m
               f(m)=Max(f(m),(1.-(muB1/x))*(FC1(I1+m+1)-FC1(I1))+
     1             (muB1/x)*(FB1(I1+m+1)-FB1(I1)))
            EndDo
         EndIf
         If(f(m).gt.fmin1(If1)) Then
          If(x.le.MeanMax) Then
C            C=ConfLev(m,x*f(m),x,istat)
            C=ConfLevNew(m,x*f(m),x,istat)
            If(Istat.eq.2) Then
C ConfLev(New) failed.
               Istat=5
               Fup=0.
               Return
            EndIf
          Else
            y=(float(m)-x*f(m))/sqrt(x*f(m))
            C=Cinf(y,f(m),Istat)
          EndIf
          If(m.lt.10) Cdbg(m)=C
          If(C.gt.CMax) Then
             CMax=C
             mmax=m
          EndIf
          If(f(m).ge.1.) Go to 10
         EndIf
      EndDo
 10   FUp=CMax - CMxinf
      If(Debug2) Then
         If(I.eq.3) Then
            Write(6,*) "ncalls, mmax, f(mmax), CMax:",
     1       ncalls,mmax,f(mmax),CMax
         EndIf
         If(NCalls.eq.0) FUpsave=FUp
         If(NCalls.eq.1 .and. FUp*FUPsave.gt.0.) Then
           Write(6,*) "RZERO will fail: ",x,muB1,CMax,mmax
         EndIf
      EndIf
      NCalls=NCalls+1
      Return
      End

c######################################################################
c                     y_vs_CLf.f
c######################################################################
      Real Function y_vs_CLf(CLin,fin,Istat)
C Computes y such that CLin=Cinf(y,fin).  .8 < CLin < .9999, .005 < f < 1
C Input: CLin=Confidence level, fin=fraction of the range, sometimes called f.
C Output: y=y_vs_CLf=y_{infinity}(CLin,fin), Istat = indicator of quality.
C         If Istat .ge. 3, the program failed, and returns y_vs_CLf=-10.
C Istat  Meaning
C   0    y_vs_CLf.in table used to interpolate
C   1    y_vs_CLf.in table used to extrapolate from f0=0.01 to low fin
C   2    Normal distribution used to extrapolate fron f0=1 to very low fin
C   3    failure: CLin > 0.9999
C   4    failure: fin > 1
C   5    failure: fin > 0.01 but CLin < 0.8
C   6    failure: unable to extrapolate for very low fin.
      Implicit None
      Real CL,f,ylow,yhigh,table(425,100),xf,xCL,FNf,fbin,
     1 CLs(425),dCL,flog,ytemp,CLin,fin,f0/.01/,f0a/.1/,Ctop/.9999/,
     2 Cbot/0.8/
C As the program was written in 2004, the lowest value of f in the
C y_vs_CLf.in table was 0.01. The table goes from CL=.8 to CL=.999.
C Sometimes f0 is too small, in which case use f0a.
C      Real*8 PPND16,C,df
      Real*8 DGAUSN,C,df
      Integer Istat,Nmin,Nf,Ntable,Ntrials,I,J,If,ICL
      Common/y_vs_CLfcom/ylow,yhigh,Nmin,Nf,NTable,Ntrials,FNf,table,
     1 CLs
      Logical first/.true./
      If(first) Then
         first=.false.
         open(20,file='y_vs_CLf.in',status='OLD',form='UNFORMATTED')
         read(20) ylow,yhigh,Nmin,Nf,NTable,Ntrials
         FNf=float(Nf)
         Do If=1,Nf
            Read(20) (table(J,If),J=1,425)
         EndDo
         Close(20)
         Do ICL=1,175
            CLs(ICL)=.799+.001*Float(ICL)
         EndDo
         Do ICL=176,425
            CLs(ICL)=.9574+.0001*Float(ICL)
         EndDo
      EndIf
      y_vs_CLf=-10. ! Default for failure
      Istat=0 ! Default for success (but 1 and 2 aren't too bad)
      If(CLin .gt. Ctop) Then
         Istat=3
         Return
      ElseIf(fin .gt. 1.) Then
         Istat=4
         Return
      ElseIf(fin.ge.0.01 .and. CLin .lt. Cbot) Then
         Istat=5
         Return
      EndIf
      If(fin.lt.0.01) Then
C If f<0.01 and CL<0.9999, extrapolate from f0 or f0a.
         Istat=1
         CL=CLin**(((1./f0)-.94)/((1./fin)-.94))
         f=f0
         If(CL.lt.Cbot) Then
            CL=CLin**(((1./f0a)-.94)/((1./fin)-.94))
            f=f0a
         EndIf
         If(CL.gt.Ctop .or. CL.lt.Cbot) Then
C For very small f, CL can get too close to 1 for the table to handle.
C Even greater than CL=.999 involves extrapolation of the table.
C But for CL>0.9999, extrapolate with the inverse of the normal distribution.
C Give a rough estimate of y_vs_CLf based on extrapolation from f=1.
C This part depends on a routine from http://lib.stat.cmu.edu/apstat/241,
C published in Applied Statistics, vol. 37, pp. 477-484, 1988.  If the
C optimum interval method is used with fewer than, say, 1000 events, I
C expect DGAUSN never to be called, so the block calling it can be removed.
           Istat=2
           df=fin
           C=CLin
C The constants in the next executable line were tuned so that the
C extrapolated results would be pretty good compared with the table
C (for df=.01) and with the extrapolation from f0=.01 for df=0.0005.
           C=C**(0.051D0/((1.D0/df)-0.946D0))
           y_vs_CLf=-sngl(DGAUSN(C,I))
C DGAUSN shouldn't fail if f>1.E-12 and CLin<0.999.  This formula
C is essentially y_vs_CLf(C,1).
           If(I.eq.1) Then
              y_vs_CLf=-10.
              Istat=6
           EndIf
           Return
         EndIf
      Else
         CL=CLin
         f=fin
      EndIf
      If(f.gt. 1.) Then
C Shouldn't be able to get here
         Write(6,*) f," is an out of range value of f in y_vs_CLf"
         stop
      EndIf
      fbin=f*FNf
      If=int(fbin)
      If(If.lt.1) If=1
      If(If.gt.Nf-1) If=Nf-1
      xf=fbin-float(If)
      If(CL.lt.Cbot .or. CL.gt.Ctop) Then
C Shouldn't be able to get here
         Write(6,*) CL," is an out of range value of CL in y_vs_CLf"
         stop
      ElseIf(CL.lt..975) Then
         ICL=int(1000.*(CL-.799)) ! ICL is from 1 to 175)
         dCL=.001
      Else
         ICL=int(10000.*(CL-.9574)) ! ICL is from 176 to 424
         dCL=.0001
      EndIf
      xCL=(CL-CLs(ICL))/dCL
      ytemp=(1.-xCL)*((1.-xf)*table(ICL,If)+xf*table(ICL,If+1))
     1 + xCL*((1.-xf)*table(ICL+1,If)+xf*table(ICL+1,If+1))
      flog=log(f)
      y_vs_CLf=(ytemp+1.7*flog)/(1.-0.3*flog)
      Return
      End

c######################################################################
c                       CMaxinfNew.f
c######################################################################
      Real Function CMaxinf(CL,Ifmin,mu)
C Uses table CMaxf.in, which was made by CMaxinfgen, to compute
C the CL confidence level value of CMaxinf for minimum f of the
C Ifmin value of fmin and for total expected number of events, mu.
C fmins has the list of values of Nfmin values of fmin.
C As of this writing, correspondence between Ifminin and fmin:
C Ifmin  fmin
C   1    0.00
C   2    0.01
C   3    0.02
C   4    0.05
C   5    0.10
C   6    0.20
C   7    0.50 = fmins(Nfmin)
C
C As of this writing, CLs consist of NCLs=40 values from .8 to .995.
C CL is not allowed to be less than CLs(1) or greater than CLs(NCls).
C If mu is too low, we can expect the result to be unreliable, but it's
C permitted.  If mu > mus(Nmus), give the same result as mu=mus(Nmus)
C or, if extrapolation is available, use A + B/sqrt(mu).  If mu<mus(Nmus),
C linearly interpolate in log(mus),CLs.
C
C Feb 14, 2007: Also read in table CMaxfLow.in, also made with CMaxinfgen.
C It tabulates mu in uniform steps of .1 starting at mu=1.6 and ending at
C mu=54.6.  Use it for mu<=mucut=?54.5, and interpolate between table entries
C N and N+1, where N=10.*(mu-1.5) for N=1 to 530.
C If there is not CMaxfLow.in, set UseTable2 to .false.
C
C Jan 2010: Also read in table CMaxfLowNew.in, made with CMaxinfgenNew.  It
C has NCLs=80 values from .8 to .9975, mu in uniform steps of .0125 starting at
C mu=1.6 and ending at mu=12.  Use it for mu < 12, ifmin=1, interpolate between
C table entries N and N+1, where N=80.*(mu-1.5875) for N=1 to 832.
C If there is not CMaxfLowNew.in, set UseTable3 to .false.
      Implicit None
      Real CL,mu
      Integer Ifmin,Nmus,NmuMax,NCLs,NCLmax,Nfmin,NfminMax,I,J,
     1 Imu,ICL,Imu24,Imu14,Imu34,Imustart,Nmus2,NmuMax2,
     2 NCL3s,NmuMax3,Nmus3
      Parameter (NCLmax=40)
      Parameter (NCL3s=80) ! For CMaxfLowNew.in
      Parameter (NfminMax=7)
      Parameter (NmuMax=50)
      Parameter (NmuMax2=730) ! 723 should be needed
      Parameter (NmuMax3=840) ! 833 should be needed
      Real CLs(NCLmax),mus(NmuMax),Table(NCLmax,NfminMax,NmuMax),
     1 fmins(NfminMax),logmu(NmuMax),lmu,xCL,xmu,sqrtmu,
     2 A(NClmax,NfminMax),B(NClmax,NfminMax),mus2(NmuMax2),
     3 Table2(NCLmax,NFminMax,NmuMax2),mucut/54.5/,CL3s(NCL3s),
     4 mucut3/12./,mus3(NmuMax3),Table3(NCL3s,NmuMax3),UseNewCMax
      Logical first/.true./,UseTable2/.true./,UseTable3/.true./,
     1 use
      Common/CMaxinfcom/NCLs,Nfmin,CLs,fmins,mus,Table,Nmus,Imu24,
     1 Imu14,Imu34,logmu,A,B,CL3s,nmus3,mus3,table3
      If(first) Then
         first=.false.
         Open(20,file='CMaxf.in',status='OLD',form='UNFORMATTED')
         Read(20) NCLs,Nfmin
         Read(20) (CLs(I),I=1,NCLs),(fmins(I),I=1,Nfmin)
         Nmus=0
         A(1,1)=0. ! A(1,1)=0 implies extrapolation data is unavailable
 10      Continue
         Nmus=Nmus+1
         read(20,End=100) mus(Nmus)
         If(mus(Nmus).eq.0.) Then
C mus(Nmus)=0 ==> no more Table entries but maybe extrapolation data.
            Read(20,End=100) ((A(ICL,J),B(ICL,J),ICL=1,NCLs),
     1       J=1,Nfmin)
            Go to 100
         EndIf
         logmu(Nmus)=log(mus(Nmus))
         Do J=1,Nfmin
           Read(20) (Table(I,J,Nmus),I=1,NCLs)
         EndDo
         Go to 10
 100     close(20)
         Nmus=Nmus-1
         Imu24=Nmus/2
         Imu14=Imu24/2
         Imu34=3*Nmus/4
C If it exists, read mus2 and Table2 in, assuming they have the same
C CLs and fmins as went with Table.  Table2 is from CMaxfLow.in. 
         If(UseTable2) Then
            Open(20,file='CMaxfLow.in',status='OLD',
     1        form='UNFORMATTED')
            Read(20)
            Read(20)
            Nmus2=0
 104        Continue
            Nmus2=Nmus2+1
            read(20,end=107) mus2(Nmus2)
            Do J=1,Nfmin
               Read(20) (Table2(I,J,Nmus2),I=1,NCLs)
            EndDo
            Go to 104
 107        Close(20)
            Nmus2=Nmus2-1
         EndIf
         If(UseTable3) Then
            Open(20,file='CMaxflowNew.in',status='OLD',
     1        form='UNFORMATTED')
            Read(20) CL3s
            Nmus3=0
 108        Continue
            Nmus3=Nmus3+1
            Read(20,end=109) mus3(Nmus3),(Table3(I,Nmus3),I=1,NCL3s)
            Go to 108
 109        Close(20)
            Nmus3=Nmus3-1
            Do I=2,112
               Table3(1,I)=0.8
            EndDo
         EndIf
      EndIf
      If(Ifmin.gt.Nfmin .or. Ifmin.lt.1) Then
          Write(6,115) Ifmin
 115      Format(I9,' is outside the allowed range of Ifmin values')
          Stop
      EndIf
      If(UseTable3 .and. mu.le.mucut3) Then
         ICL=int((CL-.7975)*400.)
         If(ICL.eq.0) ICL=1
         If(ICL.lt.1 .or. ICL.ge.NCL3s) Then
            Write(6,120) CL
            Stop
         EndIf
         xCL=(CL-CL3s(ICL))/(CL3s(ICL+1)-CL3s(ICL)) ! fraction of way to ICL+1
         xmu=80.*mu - 127. ! 80*1.6-127.=1.  Imu=xmu=1 <--> mu=1.6
         Imu=int(xmu)
         xmu=xmu-float(Imu) ! Now xmu is the fraction of the way to Imu+1
C Interpolate Table with CL between CL3s(ICL) and CL3s(ICL+1) and with
C mu between mus3(Imu) and mus3(Imu+1).
          CMaxinf=(1.-xCL)*((1.-xmu)*Table3(ICL,Imu) +
     1   xmu*Table3(ICL,Imu+1)) +
     2   xCL*((1.-xmu)*Table3(ICL+1,Imu) +
     3   xmu*Table3(ICL+1,Imu+1))
         Goto 300
      Else
C Find between what CL's we should interpolate
C Here we assume 40 CL's from .8 to .995.
        ICL=int((CL-.795)*200.)
C Allow CL as low as .795 with extrapolation.
        If(ICL.eq.0) ICL=1
        If(ICL.lt.1 .or. ICL.ge.NCLs) Then
          Write(6,120) CL
 120      Format(F9.4,' is outside the allowed range of CL values')
          Stop
        EndIF
      EndIf
C Find between what mu values we should interpolate
      If(UseTable2.and.mu.le.mucut) Then
C Initially xmu will be approximately the index of mu in the table
C We will want Imu to be the index of the table entry whose mus is
C just below mu.  We will interpolate between Imu and Imu+1.  Imu will
C be the highest integer below xmu.
         If(mu.lt.6.0) Then
            xmu=40.*mu-63. ! 40*1.6 - 63. = 1.  Imu=xmu=1 <--> mu=1.6
C For mu < 6, xmu is < 40*6-63=177.  Imu=xmu=177 is where mu=6.  So
C this part of the calculation only gets Imu up to 176, and up to Imu+1=177
C is the last of the series where spacing is .025 = 1/40.
            If(xmu.lt.1.) Then
              CMaxinf=0. ! mu is below the lowest entry in the table
              Return
            EndIf
         ElseIf(mu.lt.12) Then
            xmu=20.*mu + 57. ! 20*6+57=177=40*6-63
C For mu < 12, xmu is < 20*12+57=297.  This part of the calculation only
C gets Imu up to 296, and up to Imu+1=297 the spacing is .05 = 1/20. 
         Else
            xmu=10.*mu + 177. ! 10*12+177=297=20*12+57
C For mu < 54.6, xmu is < 10*54.6+177 = 723 = Nmus2, the number of mu entries
C in the entire Table2.   From the above comments, we should have:
C mus2(1)=1.6, mus2(177)=6, mus2(297)=12, mus2(723)=54.6.
         EndIf
         Imu=int(xmu)
C Now xmu is between Imu and Imu+1.
         xmu=xmu-float(Imu) ! Now xmu is the fraction of the way to Imu+1
         GoTo 200
      EndIf
      lmu=log(mu)
C To speed this up, first find what quarter of the range should
C be searched.  Then for simplicity over a complete binary search,
C complete the search in a dumb way.
      If(mus(Imu24).ge.mu) Then
         If(mus(Imu14).ge.mu) Then
            Imustart=2
         Else
            Imustart=Imu14
         EndIf
      Elseif(mus(Imu34).ge.mu) Then
         Imustart=Imu24
      Else
         Imustart=Imu34
      EndIf
      Do I=Imustart,Nmus
          Imu=I
          If(mus(I).ge.mu) Then
             Go to 200
          EndIf
      EndDo
C mu is greater than all the tabulated values; so use the top one or
C use A and B
      lmu=logmu(Nmus)
 200  Continue
      xCL=(CL-CLS(ICL))/(CLS(ICL+1)-CLS(ICL))
      If(mu.gt.mus(Nmus) .and. A(1,1).ne.0.) Then
C Interpolate the computed extrapolated value between CLs(ICL) and
C CLs(ICL+1).
       sqrtmu=sqrt(mu)
       CMaxinf=(1.-xCL)*(A(ICL,Ifmin) + B(ICL,Ifmin)/sqrtmu) +
     1   xCL*(A(ICL+1,Ifmin) + B(ICL+1,Ifmin)/sqrtmu)
      Elseif(UseTable2 .and. mu.le.mucut) Then
C Interpolate Table with CL between CLs(ICL) and CLs(ICL+1) and with
C mu between mus2(Imu) and mus2(Imu+1).
         CMaxinf=(1.-xCL)*((1.-xmu)*Table2(ICL,Ifmin,Imu) +
     1   xmu*Table2(ICL,Ifmin,Imu+1)) +
     2   xCL*((1.-xmu)*Table2(ICL+1,Ifmin,Imu) +
     3   xmu*Table2(ICL+1,Ifmin,Imu+1))
      Else
        Imu=Imu-1
C Interpolate Table with CL between CLs(ICL) and CLs(ICL+1) and with
C lmu between logmu(Imu) and logmu(Imu+1).
        xmu=(lmu-logmu(Imu))/(logmu(Imu+1)-logmu(Imu))
         CMaxinf=(1.-xCL)*((1.-xmu)*Table(ICL,Ifmin,Imu) +
     1   xmu*Table(ICL,Ifmin,Imu+1)) +
     2   xCL*((1.-xmu)*Table(ICL+1,Ifmin,Imu) +
     3   xmu*Table(ICL+1,Ifmin,Imu+1))
      EndIf
 300  CMaxinf=max(CMaxinf,CL)
C The above statement may be needed if mu is too low for the table.
C If the table is made with steps in mu of, say 0.025, then between
C the threshold of where CMaxinf should be defined and where the
C first non-zero entry of the table is there should be a legitimate
C return of CL.
      return
      Entry  UseNewCMax(use)
C Turn on or off use of new, more accurate table.
      UseTable3=use
      first=.true.
      UseNewCMax=0.
      Return
      End

c######################################################################
c                      ConfLevNew.f
c######################################################################
      Real Function ConfLevNew(m,x,mu,icode)
C Evaluate the probability that the maximum expected contents of an interval
C with at most m points is less than x given that the total range is mu.
C The return code, icode, is 0 if all is ok, 1 if the true value is guaranteed
C to not be above the returned value, 2 if the program can't supply any useful
C information.  ConfLev is supposed to be used only for finding confidence
C levels above 50%; if the program finds it below this, it may not bother
C finding a more accurate result.
C
C ConfLevNew changed from ConfLev.f in Jan 2010 to use CLtableNew.in
C when that table made with more statistics and finer binning is useable.
C Otherwise it uses CLtable.in via the old ConfLev.f
      Implicit None
      Integer m,icode
      Real x,mu
      Integer N,Nmeans,NCLs
C      Parameter(N=50)
C      Parameter(Nmeans=41)
C      Parameter(NCLs=22)
      Parameter(N=5) ! Jan 2010
      Parameter(Nmeans=51) ! Jan 2010 (From CtableNew where L is 0-50)
      Parameter(NCLs=44) ! Jan 2010
C      Common/CLtab/CL,Mean,xcl,Nxcl,meanlog ! For aid in debugging
      Common/CLtabNew/CL,Mean,xcl,Nxcl,meanlog ! For aid in debugging, save
      Real CL(NCLs),xcl(0:N,NCLs,Nmeans),mean(Nmeans),ConfLev
      Real meanlog(Nmeans),mulog,xcl2(NCLs+1),clxmu,GAMDIS,
     x CL2(NCLs+1),UseNewConfL
      Logical first/.true./,UseNew/.true./,use
      Integer I,J,K,L,Nxcl(0:N,Nmeans),Nxcl2
      If(first) Then
C Input the table.  xcl(m,J,I) is the maximum interval containing m points
C corresponding to mu=mean(I) and confidence level CL(J).  If Nxcl(m,I)=0,
C then the m is so large compared with mu that one hardly ever gets m
C events; so if x<mu, the confidence level is very small.  I assume that if
C xcl(m,J,I) is meaningful then so is xcl(m,J,I+1) and xcl(m,J-1,I)
C (unless J=1).
       First=.false.
       If(UseNew) Then
        Open(21,file='CLtableNew.in',status='OLD',form='UNFORMATTED') ! Jan 2010
        Read(21) CL ! There are places where I assume CL(2) .le. .5
        Do I=1,Nmeans
          Read(21) mean(I),(Nxcl(K,I),K=0,N)
          Do K=0,N
             If(Nxcl(K,I).gt.0) Read(21) (xcl(K,J,I),J=1,Nxcl(K,I))
          EndDo
          meanlog(I)=log(mean(I))
        EndDo
        Close(21)
       EndIf
      EndIf
      If(mu.gt.12. .or. m.gt.5 .or. .not. UseNew) Then
         ConfLevNew = ConfLev(m,x,mu,icode)
         Return
      EndIf
C Make some simple checks on reasonableness of the input parameters.  If
C they are unreasonable, either return a reasonable output anyway, or give up.
      If(mu.lt.0. .or. m.lt.0) Then
         Go to 1000
      ElseIf(x.gt.1.00001*mu) Then
         ConfLevNew=1.
      ElseIf(x.le.0.) Then
         ConfLevNew=0.
      ElseIf(mu.lt.1. .or. mu.ge.mean(Nmeans) .or. m.gt.N) Then
C The table hasn't the needed information, but at least we know that the
C answer is smaller than the confidence level for x=mu-.
         ConfLevNew=GAMDIS(mu,float(m+1))
         Go to 2000
      Else ! The table might include this m,mu
C Find which mean(I) is just below mu.  Since mu is less than mean(Nmeans),
C the I must be less than Nmeans; so there is information for I+1.
         mulog=log(mu) ! Since mu.ge.1, mulog is at least 0.
C         I=10.*mulog + 1.     ! mu = exp(.1*L) L=0,1,...  I=L+1
         I=int(20.*mulog + 1.) ! mu = exp(.05*L) Jan 2010
         Nxcl2=min(Nxcl(m,I),Nxcl(m,I+1))
         If(Nxcl2.lt.2) Then
C The m must be too large for the mu; CL is very low.
            ConfLevNew=CL(2)
            Go to 2000
         EndIf
C Nxcl(m,I) is at least 2 after this point.
C Find clxmu, the confidence level for x=mu-.
         clxmu=GAMDIS(mu,float(m+1))
C Return clxmu if x is equal to mu to within one part in 100,000.
         If(x .gt. .99999*mu) Then
            ConfLevNew=clxmu
            Go to 500
         EndIf
C Interpolate table entries to apply to this value of mu
         Do J=1,Nxcl2
            xcl2(J)=xcl(m,J,I) + (mulog-meanlog(I))*
     x      (xcl(m,J,I+1)-xcl(m,J,I))/(meanlog(I+1)-meanlog(I))
            CL2(J)=CL(J)
         EndDo
         xcl2(Nxcl2+1)=mu
         CL2(Nxcl2+1)=clxmu
C Interpolate xcl2 to find ConfLev at x.  K = 2 or 3 xcl2's are used,
C beginning with xcl2(J).  xcl2(1) and xcl2(2) must exist to get here.
C If x is before xcl2(2), then xcl2(1), xcl2(2), and xcl2(3) are
C used.  Analogously, if x is after xcl2(Nxcl(m,I)), use that xcl2, the one
C above it, and the one below it.  Otherwise use the two xcl2's 
C between which x lies, and the nearest other one to x.
         K=3
         If(x.lt.xcl2(1)) Then
C Only linearly extrapolate before xcl2(1); otherwise the parabola could
C result in smaller x getting larger confidence level
            J=1
            K=2
         ElseIf(x.le.xcl2(2)) Then
            J=1
C         ElseIf(Nxcl(m,I).eq.NCLs .and. x.gt.xcl2(NCLs)) Then
C Only linearly extrapolate beyond xcl2(NCLs); otherwise the parabola could
C result in larger x getting smaller confidence level.
C            J=NCLs
C            K=2
         ElseIf(x.ge.xcl2(Nxcl2)) Then
            J=Nxcl2-1
         Else ! x is between xcl2(2) and xcl2(Nxcl2); Nxcl2>3.
            Do L=3,Nxcl2 ! Find the first xcl2 that's above x
               If(x.le.xcl2(L)) Then
                  If(xcl2(L+1)-x .lt. x-xcl2(L-2)) Then
                     J=L-1
                  Else
                     J=L-2
                  EndIf
                  Go to 100
               EndIf
            EndDo
 100        Continue
         EndIf
C Maybe at this point I should improve the xcl2 that are used: quadratically
C interpolate the xcl over three neighboring I.  I tried it; no improvement.
C Find JJ, the first of the triplet used for quadratic interpolation
C in improving the xcl2's.  Normally use I, I+1, and I+2 for the interpolation,
C for if the xcl exist for I and I+1, then they do for I+2 unless I+2>Nmeans.
C         If(I+2.gt.Nmeans) Then
C            JJ=I-1
C         Else
C            JJ=I
C         EndIf
C         Do L=1,K
C            LL=J+L-1
C            If(LL.LE.Nxcl(m,I)) Then
C             xcl2(LL)=xcl(m,LL,JJ) + ((mulog-meanlog(JJ))/(meanlog(JJ+1)
C     x        - meanlog(JJ+2))) * ( (xcl(m,LL,JJ+1)-xcl(m,LL,JJ))*
C     x        (mulog-meanlog(JJ+2))/(meanlog(JJ+1)-meanlog(JJ)) +
C     x        (xcl(m,LL,JJ+2)-xcl(m,LL,JJ))*(meanlog(JJ+1)-mulog)/
C     x        (meanlog(JJ+2)-meanlog(JJ)) )
C            EndIf
C         EndDo
         If(K.eq.2) Then ! Linearly interpolate
            ConfLevNew=CL2(J) + (CL2(J+1)-CL2(J))*(x - xcl2(J))/
     x       (xcl2(J+1) - xcl2(J))
         Else ! Quadratically interpolate
            ConfLevNew=CL2(J) + ((x-xcl2(J))/(xcl2(J+1)-xcl2(J+2))) * (
     x       (CL2(J+1)-CL2(J))*(x-xcl2(J+2))/(xcl2(J+1)-xcl2(J)) +
     x       (CL2(J+2)-CL2(J))*(xcl2(J+1)-x)/(xcl2(J+2)-xcl2(J)) )
         EndIf
         If(ConfLevNew.lt.0.) ConfLevNew=0.
         If(ConfLevNew.gt.clxmu) ConfLevNew=clxmu
      EndIf ! End of the case in which the table can include the m and mu.
 500  icode=0
      return
 1000 ConfLevNew=-1.
      icode=2
      return
 2000 icode=1
      return
      Entry UseNewConfL(use)
C Turn on or off use of new, more accurate table.
      UseNew=use
      First=.true.
      UseNewConfL=0.
      Return
      end

c**********************************************************************
c                       ConfLev.f
c**********************************************************************
      Real Function ConfLev(m,x,mu,icode)
C ConfLev(m,x,mu,icode) = C_m(x,mu):
C Evaluate the probability that the maximum expected contents of an interval
C with at most m points is less than x given that the total range is mu.
C The return code, icode, is 0 if all is ok, 1 if the true value is guaranteed
C to not be above the returned value, 2 if the program can't supply any useful
C information.  ConfLev is supposed to be used only for finding confidence
C levels above 50%; if the program finds it below this, it may not bother
C finding a more accurate result.
C
C Uses GAMDIS, which requires cern libraries libmathlib and libkernlib
C Needs table CLtable.txt.
C
      Implicit None
      Integer m,icode
      Real x,mu
C
C Tabulated information
      Integer N,Nmeans,NCLs,NCMaxmu
      Parameter(N=50)
      Parameter(Nmeans=41)
      Parameter(NCLs=22)
      Parameter(NCMaxmu=70)
      Common/CLtab/CL,Mean,xcl,Nxcl,muval,CMaxval,meanlog
      Real CL(NCLs),xcl(0:N,NCLs,Nmeans),mean(Nmeans),meanlog(Nmeans)
      Real muval(NCMaxmu),CMaxval(NCMaxmu)
      Integer Nxcl(0:N,Nmeans)
C
      Real mulog,xcl2(NCLs+1),clxmu,GAMDIS,
     x CL2(NCLs+1)
      Integer Nxcl2
      Logical first/.true./
      Integer I,J,K,L
      If(first) Then
C Input the table.  xcl(m,J,I) is the maximum interval containing m points
C corresponding to mu=mean(I) and confidence level CL(J).  If Nxcl(m,I)=0,
C then the m is so large compared with mu that one hardly ever gets m
C events; so if x<mu, the confidence level is very small.  I assume that if
C xcl(m,J,I) is meaningful then so is xcl(m,J,I+1) and xcl(m,J-1,I)
C (unless J=1).
        First=.false.
        Open(21,file='CLtable.txt',status='OLD',form='FORMATTED')
        Read(21,10) muval
        Read(21,10) CMaxval
 10     Format(70F8.4)
        Read(21,14) CL ! There are places where I assume CL(2) .le. .5
 14     Format(22F8.5)
        Do I=1,Nmeans
          Read(21,16) mean(I),(Nxcl(K,I),K=0,N)
 16       Format(F9.5,51I3)
          Do K=0,N
             If(Nxcl(K,I).gt.0) Read(21,17) (xcl(K,J,I),J=1,Nxcl(K,I))
 17          Format(22F9.5)
          EndDo
          meanlog(I)=log(mean(I))
        EndDo
        Close(21)
      EndIf
C Make some simple checks on reasonableness of the input parameters.  If
C they are unreasonable, either return a reasonable output anyway, or give up.
      If(mu.lt.0. .or. m.lt.0) Then
         Go to 1000
      ElseIf(x.gt.1.00001*mu) Then
         ConfLev=1.
      ElseIf(x.le.0.) Then
         ConfLev=0.
      ElseIf(mu.lt.1. .or. mu.ge.mean(Nmeans) .or. m.gt.N) Then
C The table hasn't the needed information, but at least we know that the
C answer is smaller than the confidence level for x=mu-.
         ConfLev=GAMDIS(mu,float(m+1))
         Go to 2000
      Else ! The table might include this m,mu
C Find which mean(I) is just below mu.  Since mu is less than mean(Nmeans),
C the I must be less than Nmeans; so there is information for I+1.
         mulog=log(mu) ! Since mu.ge.1, mulog is at least 0.
         I=int(10.*mulog + 1.)
         Nxcl2=min(Nxcl(m,I),Nxcl(m,I+1))
         If(Nxcl2.lt.2) Then
C The m must be too large for the mu; CL is very low.
            ConfLev=CL(2)
            Go to 2000
         EndIf
C Nxcl(m,I) is at least 2 after this point.
C Find clxmu, the confidence level for x=mu-.
         clxmu=GAMDIS(mu,float(m+1))
C Return clxmu if x is equal to mu to within one part in 100,000.
         If(x .gt. .99999*mu) Then
            ConfLev=clxmu
            Go to 500
         EndIf
C Interpolate table entries to apply to this value of mu
         Do J=1,Nxcl2
            xcl2(J)=xcl(m,J,I) + (mulog-meanlog(I))*
     x      (xcl(m,J,I+1)-xcl(m,J,I))/(meanlog(I+1)-meanlog(I))
            CL2(J)=CL(J)
         EndDo
         xcl2(Nxcl2+1)=mu
         CL2(Nxcl2+1)=clxmu
C Interpolate xcl2 to find ConfLev at x.  K = 2 or 3 xcl2's are used,
C beginning with xcl2(J).  xcl2(1) and xcl2(2) must exist to get here.
C If x is before xcl2(2), then xcl2(1), xcl2(2), and xcl2(3) are
C used.  Analogously, if x is after xcl2(Nxcl(m,I)), use that xcl2, the one
C above it, and the one below it.  Otherwise use the two xcl2's 
C between which x lies, and the nearest other one to x.
         K=3
         If(x.lt.xcl2(1)) Then
C Only linearly extrapolate before xcl2(1); otherwise the parabola could
C result in smaller x getting larger confidence level
            J=1
            K=2
         ElseIf(x.le.xcl2(2)) Then
            J=1
C         ElseIf(Nxcl(m,I).eq.NCLs .and. x.gt.xcl2(NCLs)) Then
C Only linearly extrapolate beyond xcl2(NCLs); otherwise the parabola could
C result in larger x getting smaller confidence level.
C            J=NCLs
C            K=2
         ElseIf(x.ge.xcl2(Nxcl2)) Then
            J=Nxcl2-1
         Else ! x is between xcl2(2) and xcl2(Nxcl2); Nxcl2>3.
            Do L=3,Nxcl2 ! Find the first xcl2 that's above x
               If(x.le.xcl2(L)) Then
                  If(xcl2(L+1)-x .lt. x-xcl2(L-2)) Then
                     J=L-1
                  Else
                     J=L-2
                  EndIf
                  Go to 100
               EndIf
            EndDo
 100        Continue
         EndIf
C Maybe at this point I should improve the xcl2 that are used: quadratically
C interpolate the xcl over three neighboring I.  I tried it; no improvement.
C Find JJ, the first of the triplet used for quadratic interpolation
C in improving the xcl2's.  Normally use I, I+1, and I+2 for the interpolation,
C for if the xcl exist for I and I+1, then they do for I+2 unless I+2>Nmeans.
C         If(I+2.gt.Nmeans) Then
C            JJ=I-1
C         Else
C            JJ=I
C         EndIf
C         Do L=1,K
C            LL=J+L-1
C            If(LL.LE.Nxcl(m,I)) Then
C             xcl2(LL)=xcl(m,LL,JJ) + ((mulog-meanlog(JJ))/(meanlog(JJ+1)
C     x        - meanlog(JJ+2))) * ( (xcl(m,LL,JJ+1)-xcl(m,LL,JJ))*
C     x        (mulog-meanlog(JJ+2))/(meanlog(JJ+1)-meanlog(JJ)) +
C     x        (xcl(m,LL,JJ+2)-xcl(m,LL,JJ))*(meanlog(JJ+1)-mulog)/
C     x        (meanlog(JJ+2)-meanlog(JJ)) )
C            EndIf
C         EndDo
         If(K.eq.2) Then ! Linearly interpolate
            ConfLev=CL2(J) + (CL2(J+1)-CL2(J))*(x - xcl2(J))/
     x       (xcl2(J+1) - xcl2(J))
         Else ! Quadratically interpolate
            ConfLev=CL2(J) + ((x-xcl2(J))/(xcl2(J+1)-xcl2(J+2))) * (
     x       (CL2(J+1)-CL2(J))*(x-xcl2(J+2))/(xcl2(J+1)-xcl2(J)) +
     x       (CL2(J+2)-CL2(J))*(xcl2(J+1)-x)/(xcl2(J+2)-xcl2(J)) )
         EndIf
         If(ConfLev.lt.0.) ConfLev=0.
         If(ConfLev.gt.clxmu) ConfLev=clxmu
      EndIf ! End of the case in which the table can include the m and mu.
 500  icode=0
      return
 1000 ConfLev=-1.
      icode=2
      return
 2000 icode=1
      return
      end

c**********************************************************************
c                       Cinf.f
c**********************************************************************
      Real Function Cinf(y,fin,Istat)
C Calculates C_\infty from a table.
C Input: y = deviation, fin = fraction of the range
C Output: Cinf=C_\infty(y,fin), Istat = return status code
C Istat  Meaning
C   0    Result interpolated from table ymintable.in
C   1    Result extrapolated from f0=.01 (lowest f in the table)
C   2    y especially low.  Returns Cinf=1.
C   3    y especially high.  Returns Cinf=0.
C   4    Result estimated using derfc (not implemented)
C   5    failure: fin > 1
C   6    failure: fin < 1.E-10
      Implicit none
      Integer Nf,Nmin,Ntrials,Ntable,If,Ibin,Istat
      Real y,ytemp,f,ylow,yhigh,flog,Table(2000,100),FNf,
     1 fbin,ybin,dfbin,dIbin,dy,fin,f0/.01/
      Logical first/.true./
      Common/Cinfcom/Nf,Ntable,ylow,yhigh,dy,FNf,Table
      If(first) Then
       first=.false.
       Open(50,file='ymintable.in',status='OLD',form='UNFORMATTED')
       Read(50) ylow,yhigh,Nmin,Nf,NTable,Ntrials
       Do If=1,Nf
          Read(50) (Table(Ibin,If),Ibin=1,Ntable)
       EndDo
       dy=(yhigh-ylow)/float(Ntable)
       FNf=float(Nf)
       Close(50)
      EndIf
      Istat=0 ! Default for success
      Cinf=1. ! Default for failure
      f=fin
      If(f.gt.1.) Then
         Istat=4
         Return
      EndIf
      If(f.lt.1.E-10) Then
         Istat=5
         Return
      EndIf
      If(f.lt.0.01) Then
         Istat=1
         f=f0
      EndIf
      flog=log(f)
      ytemp=y*(1. - 0.3*flog) - 1.7*flog
      If(ytemp.lt.ylow) Then
         Istat=2
         Cinf=1.
         Return
      Elseif(ytemp.gt.yhigh) Then
         Istat=3
         Cinf=0.
         Return
      EndIf
      fbin=f*FNf
      If=int(fbin)
      If(If.lt.1) If=1
C      If(If.gt.Nf-2) If=Nf-2
      If(If.gt.Nf-1) If=Nf-1
      dfbin=fbin-Float(If)
      ybin=(ytemp-ylow)/dy
      Ibin=int(ybin)
      If(Ibin.lt.2) Ibin=2
      If(Ibin.gt.Ntable-1) Ibin=Ntable-1
      dIbin=ybin-Float(Ibin)
C To check the following interpolation formula, verify that it gives
C the correct 4 table entries when dfbin is close to 0 and 1 and when
C dIbin is close to 0 and 1.  Table(Ibin,If)=Cinf evaluated at
C ytemp=ylow+dy*Ibin, f=If/FNf.
      Cinf=(1.-dfbin)*(dIbin*Table(Ibin+1,If)+(1.-dIbin)*Table(Ibin,If))
     1    +dfbin*(dIbin*Table(Ibin+1,If+1)+(1.-dIbin)*Table(Ibin,If+1))
      If(Istat.eq.1) Cinf=Cinf**((1./fin - 0.94)/(1./f0 - 0.94))
      Return
      End

