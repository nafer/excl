module SomeData
  integer, parameter ::&
       maxnd=10000,&   ! upper limit of extracted wimp like events
       maxdets=10     ! largest number of detectors
  integer :: nd(maxdets), numdets

  real*8 :: &
       !event energies, first and last is experimental range
       energy(0:maxnd+1,maxdets),&
       efrom(maxdets), eto(maxdets),& ! experimental range
       beta_t, alpha_max(0:maxnd*maxdets+1)

  ! beta_t: expected wimp counts in range [0,1] of transformed energy 
  !        coordinates for unit cross section in all detectors
  ! alpha_max is maximum of expected wimp counts of all interval with 
       ! index events inside for unity cross section

end module SomeData

!**********************************************************************
!                          module eff_tables
!**********************************************************************
module eff_tables
  use SomeData, only : maxdets, numdets

  integer, parameter :: &
       max_cut_bins=10000,&   ! limit of number of bin for cut efficiency table
       max_trig_bins=10000,&  ! maximum allowed bins in trigger efficiency table 
       max_remove_bins=10    ! maximum number of energy invervals to remove 
  real*8 :: &
       ecut(max_cut_bins, maxdets),&     ! energy
       cut_eff(max_cut_bins, maxdets),&  ! cut efficiency for energy
       Eremove_from(max_remove_bins,maxdets),&  ! energy interval to removed
       Eremove_to(max_remove_bins,maxdets),&     ! energy interval to remove
       etrig(max_trig_bins,maxdets),&    ! energies for trigger efficiencies
       trig_eff(max_trig_bins,maxdets)   ! trigger efficiencies 

       integer :: &
            cut_bins(maxdets),&
            remove_bins(maxdets),&
            trig_bins(maxdets)
end module eff_tables

!**********************************************************************
!                         module yield_range
!**********************************************************************
module yield_range
  ! target type and 
  ! parameters for defining accepted yields (relevant only for targets
  ! with light)
  ! fraction(1) defines upper yield limit, fraction(2) the lower limit
  real*8 :: fraction(2)=(/0.5, 0.005/) 
  integer :: type(2)=(/3,2/)           ! oxygen, W band
contains
  subroutine set_upper_acceptance_band(number)
    integer, intent(in) :: number
    type(1)=number
  end subroutine set_upper_acceptance_band

  integer function get_upper_acceptance_band()
    get_upper_acceptance_band=type(1)
  end function get_upper_acceptance_band

  subroutine set_lower_acceptance_band(number)
    integer, intent(in) :: number
    type(2)=number
  end subroutine set_lower_acceptance_band
  
  integer function get_lower_acceptance_band()
    get_lower_acceptance_band=type(2)
  end function get_lower_acceptance_band
end module yield_range
