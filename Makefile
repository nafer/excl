# Generic GNUMakefile
 
# Just a snippet to stop executing under other make(1) commands
# that won't understand these lines
ifneq (,)
This makefile requires GNU Make.
endif

#LIBS=-L/opt/Programs/graphx/lib -lgralb -lX11
#FC=/usr/bin/gfortran -cpp -D_GRAPHX_AVAILABLE_

#Uncommment next two lines if graphx library is not available on your system
LIBS=
FC=gfortran -cpp

FC90=$(FC) -Wall -fbackslash -O2


OJS = c0.o dqagse_stuff.o nr_stuff.o yellin.o CERN_Stuff.o utils.o target_type.o modules.o \
acceptance_routines.o get_recoil_spectrum.o  excl.o

#make executable
excl: $(OJS)
	$(FC) -o excl  -Wall -frecord-marker=4 $(OJS) $(LIBS)

#Target lines to make the object modules
c0.o: c0.for
	$(FC) -c -fdefault-real-8 c0.for

nr_stuff.o: nr_stuff.f
	$(FC) -fdefault-real-8 -c nr_stuff.f

dqagse_stuff.o: dqagse_stuff.f
	$(FC) -c dqagse_stuff.f

yellin.o: yellin.f
	$(FC) -Wall -frecord-marker=4 -fbackslash -c yellin.f

CERN_Stuff.o: CERN_Stuff.f
	$(FC) -c CERN_Stuff.f

target_type.o: target_type.f90
	$(FC90) -c target_type.f90

modules.o: modules.f90
	$(FC90) -c modules.f90

acceptance_routines.o: acceptance_routines.f90
	$(FC90) -c acceptance_routines.f90

get_recoil_spectrum.o: get_recoil_spectrum.f90
	$(FC90) -c get_recoil_spectrum.f90

utils.o: utils.f90
	$(FC90) -c utils.f90

excl.o: excl.f90
	$(FC90) -c -cpp excl.f90

clean:
	rm -vf *.mod *.o *~
