!**********************************************************************
!                module target_type
!**********************************************************************
module target_type
  integer, parameter :: max_target_type=7, max_nuc=3

  integer, private, parameter :: name_length=8
  character(len=name_length), private, parameter :: target_name(max_target_type)=(/&
       'CaWO4  ','Al2O3  ','Ge     ','Si     ','LiAlO2 ', 'Diamond','Al2O3  '/)

  !number of different target nuclei
  integer, private, parameter :: num_nuc_v(max_target_type)=(/3,2,1,1,3,1,2/)
  logical, private :: has_light(max_target_type)=&
       (/.true., .true., .false., .false., .false., .false., .false./)

  !number of distinguishable bands in energy light band
  integer, private :: bands(max_target_type)=(/3,1,0,0,0,0,0/)
  ! number of the band to which nucleus belongs (scint. target). Possibility to have
  ! fewer number of resolved bands than types of nuclei requires this gymnastics
  integer, private :: band_of_nucleus(max_nuc,max_target_type)=reshape((/&
       1,2,3, & ! CaWO4
       1,1,1, & ! Sapphire
       0,0,0, &
       0,0,0, &
       0,0,0, &
       0,0,0, &
       0,0,0  &  ! sapphire non scintillating
       /),shape(band_of_nucleus))
 
  ! upper and lower bands to use for acceptance calculation (only scint. target)
  integer, private :: upper_band(max_target_type)=(/3,1,0,0,0,0,0/)
  integer, private :: lower_band(max_target_type)=(/2,1,0,0,0,0,0/)

  
  ! number of nuclei of each type in structural unit
  integer, private, parameter :: numt_v(max_nuc, max_target_type)=reshape((/&
       1, 1, 4,& ! CaWO4
       2, 3, 0,& ! Al2O3
       1, 0, 0,& ! Ge
       1, 0, 0,& ! Si
       1, 1, 2,& ! LiAl02
       1, 0, 0,& ! Diamond
       2, 3, 0 & ! Sapphire as non scintillating target
       /), shape(numt_v))

  ! Atomic number of nuclei in target  
  real*8, private, parameter :: A(max_nuc,max_target_type)=&
       reshape((/&
       40.0780d0, 183.840d0, 15.9994d0,  &  ! target 1: CaWO4
       26.9815d0, 15.9994d0,  0.d0,      &  ! target 2: Al203
       72.630d0,  0.d0,       0.d0,      &  ! target 3: Ge (natural)
       28.085d0,  0.d0,       0.d0,      &  ! target 4: Si
       6.94d0  , 26.9815d0,   15.9994d0, &  ! target 5: LiAl02
       12.011d0, 0.d0,        0.d0,      &  ! target 6: Diamond
       26.9815d0, 15.9994d0,  0.d0       &  ! target 2: Al203 non scintillating
       /), shape(A))

  ! atomic mass of structural unit in target
  real*8, private, parameter ::amass_v(max_target_type)=(/&
       sum(A(1:,1)*numt_v(1:,1)), &
       sum(A(1:,2)*numt_v(1:,2)), &
       sum(A(1:,3)*numt_v(1:,3)), &
       sum(A(1:,4)*numt_v(1:,4)), &
       sum(A(1:,5)*numt_v(1:,5)), &
       sum(A(1:,6)*numt_v(1:,6)), &
       sum(A(1:,7)*numt_v(1:,7))  &
       /)

  ! weight fraction of nucleus type
  real*8, private, parameter :: wf(max_nuc, max_target_type) = reshape((/&
       numt_v(1,1)*A(1,1)/amass_v(1), numt_v(2,1)*A(2,1)/amass_v(1), numt_v(3,1)*A(3,1)/amass_v(1),&
       numt_v(1,2)*A(1,2)/amass_v(2), numt_v(2,2)*A(2,2)/amass_v(2), numt_v(3,2)*A(3,2)/amass_v(2),&
       numt_v(1,3)*A(1,3)/amass_v(3), numt_v(2,3)*A(2,3)/amass_v(3), numt_v(3,3)*A(3,3)/amass_v(3),&
       numt_v(1,4)*A(1,4)/amass_v(4), numt_v(2,4)*A(2,4)/amass_v(4), numt_v(3,4)*A(3,4)/amass_v(4),&
       numt_v(1,5)*A(1,5)/amass_v(5), numt_v(2,5)*A(2,5)/amass_v(5), numt_v(3,5)*A(3,5)/amass_v(5),&
       numt_v(1,6)*A(1,6)/amass_v(6), numt_v(2,6)*A(2,6)/amass_v(6), numt_v(3,6)*A(3,6)/amass_v(6),&
       numt_v(1,7)*A(1,7)/amass_v(7), numt_v(2,7)*A(2,7)/amass_v(7), numt_v(3,7)*A(3,7)/amass_v(7) &
       /), shape(wf))

  integer, private :: this_target=1          ! target selector, default CaWO4
contains

  real*8 function An(nuc)
    An=A(nuc,this_target)
  end function An

  real*8 function amass()
    amass=amass_v(this_target)
  end function amass

  real*8 function weight_fraction(nuc)
    weight_fraction=wf(nuc,this_target)
  end function weight_fraction

  integer function numt(nuc)
    numt=numt_v(nuc,this_target)
  end function numt

  integer function num_nuc()
    num_nuc=num_nuc_v(this_target)
  end function num_nuc

  logical function use_light()
    use_light=has_light(this_target)
  end function use_light

  subroutine set_no_light()
    has_light(this_target)=.false.
    band_of_nucleus(1:3,this_target)=0
    upper_band(this_target)=0
    lower_band(this_target)=0
  end subroutine set_no_light

  integer function get_target_type()
    get_target_type=this_target
  end function get_target_type

  subroutine set_target_type(type)
    integer, intent(in) :: type
    this_target=type
  end subroutine set_target_type

  character(len=name_length) function get_target_name()
    get_target_name=target_name(this_target)
  end function get_target_name

  integer function get_number_of_bands()
    get_number_of_bands=bands(this_target)
  end function get_number_of_bands

  integer function get_upper_band()
    get_upper_band=upper_band(this_target)
  end function get_upper_band

  integer function get_lower_band()
    get_lower_band=lower_band(this_target)
  end function get_lower_band

  integer function get_band_of_nucleus(nuc)
    use utils, only : ceprint
    get_band_of_nucleus=band_of_nucleus(nuc, this_target)
    if(.not.has_light(this_target)) then
       call ceprint("#Error in get_band_of_nucleus:\n&
            & Function called for non scintillating target#")
    endif
  end function get_band_of_nucleus
end module target_type
