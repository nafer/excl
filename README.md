excl allows to calculate dark matter limits with the Yellin method for multiple detectors. excl without command line parameters provides extended help on usage. For running it, you will need a task file which excplains where to find the 3 data files needed for each detector.

Installation normally requires the graphx library available in 
/opt/Programs/graphx.
Uncommenting two lines in the Makefile allows an installation without external
libraries. Installation should work on any system where gfortran is available.
The program prefers to live in /opt/Programs/excl.
Typing make in this directory should be enough to make the executable excl. For installing excl, you may link it in your private bin with the command
ln -s /opt/Programs/excl/excl ~/bin/excl
