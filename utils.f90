module utils
  character, parameter :: esc*1=char(27)
  character, parameter :: bold_green*9=esc//"[1m"//esc//"[32m"
  character, parameter :: green*5=esc//"[32m"
  character, parameter :: bold_yellow*9=esc//"[1m"//esc//"[33m"
  character, parameter :: yellow*5=esc//"[33m"
  character, parameter :: bold_magenta*9=esc//"[1m"//esc//"[35m"
  character, parameter :: magenta*5=esc//"[35m"
  character, parameter :: bold_red*9=esc//"[1m"//esc//"[31m"
  character, parameter :: red*5=esc//"[31m"   
  character, parameter :: white*5=esc//"[37m"
  character, parameter :: black*5=esc//"[30m"
  !  character, parameter :: reset*4=esc//"[0m"

  !reset bg black and fg white and reset bold
  character, parameter, private :: reset*14=esc//"[0m"//esc//"[40m"//esc//"[37m"
  character(len=*),parameter,private :: supported_color(10)=&
       (/bold_green, green, bold_yellow, yellow, bold_magenta, magenta,&
       bold_red, red, white, black/)
  external IsFlagSet
  logical IsFlagSet
contains

  subroutine set_background_and_forground_colors()
    print '(a,$)',reset
  end subroutine set_background_and_forground_colors

  subroutine reset_colors()
    print'(a,$)',esc//"[0m"
  end subroutine reset_colors

  !**********************************************************************
  !                      cprint
  !**********************************************************************
  subroutine cprint(text, which_color, new_line)
    ! print text bracketed by # in color, # are not printed
    implicit none
    character(len=*), intent(in) :: text
    character(len=*), optional, intent(in) :: which_color
    logical, optional, intent(in) :: new_line
    integer :: i, n
    logical col, nl
    character*10 color
    color=bold_green
    if(present(which_color)) then
       do i=1,size(supported_color)
          if(which_color.eq.supported_color(i)) then
             color=supported_color(i)
             exit
          endif
       end do
    endif

    nl=.true.
    if(present(new_line)) nl=new_line ! Zeilenvorschub nach Textausgabe

    col=.false.
    n=0
    do i=1,len_trim(text)
       if(text(i:i)=='#') then
          col=.not.col
          if(col) then
             print '(a,$)',trim(color)
          else
             print '(a,$)',reset
          endif
       else
          if(n>0) then
             print '(a,$)',text(i:i)
          else
             print '(1x,a,$)',text(i:i)
          endif
          n=n+1
       endif
    end do
    if(nl) print *
  end subroutine cprint

  subroutine cnprint(txt)
    character(len=*) txt
    call cprint(txt,green,.false.)
  end subroutine cnprint

  subroutine ceprint(txt)
    character(len=*), intent(in) :: txt
    call cprint(txt,bold_red,.true.)
  end subroutine ceprint


  subroutine cenprint(txt)
    character(len=*), intent(in) :: txt
    call cprint(txt,bold_red,.false.)
  end subroutine cenprint

  !********************************************************************
  !                   PrintHelp
  !********************************************************************
  subroutine PrintHelp(hlpfile)
    implicit none
    character*(*) hlpfile
    character(len=*), parameter :: progdir="/opt/Programs/excl/"
    CHARACTER ANS*1, txt*80, color*10
    integer ios, lun
    ans=" "
    open(newunit=lun,file=trim(progdir)//trim(hlpfile),iostat=ios)
    if(ios.ne.0) then
       print *,"Error opening file "//trim(progdir)//trim(hlpfile)
       return
    endif
    do
       read(lun,"(A)",iostat=ios) txt
       if(index(txt,"*****").gt.0) then
          color=bold_magenta
       elseif(index(txt,"-----").gt.0) then
          color=bold_yellow
       else
          color=bold_green
       endif
       if(ios.ne.0) exit
       call cprint(trim(txt),color)
    end do
    close(unit=lun)
    return
  end subroutine PrintHelp

  !***************************************************************************
  !     aski, askf, askd, aska fragen mit dem 'text' nach der Eingabe.
  !     Der vorgeschlagene Wert bei <cr> beibehalten.
  !***************************************************************************
  subroutine aski(text,nvar)
    implicit none
    CHARACTER(len=*), intent(in) :: text
    integer nvar
    integer len, ios
    character*20 an*20
    do
       write(an,'(i10)') nvar
       call cnprint(trim(text)//"  "//trim(an)//" ?  ")

       read(*,'(a)',iostat=ios) an
       len=Len_Trim(an)
       if(len > 0) then
          read(an(1:len),*,iostat=ios) nvar
          if(ios.ne.0) then
             call ceprint('#illegal input#')
             cycle
          endif
       endif
       exit
    end do
    return
  end subroutine aski

  subroutine askf(text,xvar)
    implicit none
    character(len=*), intent(in) :: text
    character*20 an
    real xvar
    integer len,ios
    do
       write(an,"(f12.4)") xvar
       call cnprint(trim(text)//" "//trim(an)//" ?  ")
       read(*,'(a)',iostat=ios) an
       len=len_trim(an)
       if(len.gt.0) then
          read(an(1:len),*,iostat=ios) xvar
          if(ios.ne.0) then
             call ceprint("#WRONG INPUT !!!!#")
             cycle
          endif
       end if
       exit
    end do
    return
  end subroutine askf

  subroutine askd(text,xdvar)
    implicit none
    character*(*) text
    real*8 xdvar
    real xvar
    xvar=sngl(xdvar)
    call askf(text,xvar)
    xdvar=xvar
  end subroutine askd

  subroutine aska(text,avar)
    implicit none
    character(len=*), intent(in) :: text
    character(len=*) avar
    integer len, ios
    character*80 an
    call cnprint(trim(text)//"  "//trim(avar)//" ?  ")
    read(*,"(a)",iostat=ios)  an
    len=len_trim(an)
    if(len>0) then
       call MakeUpperCase(an)
       avar=an
    endif
    return
  end subroutine aska


  subroutine ask_text(text,avar,upper)
    ! upper selects whether answer is converted to upper case
    implicit none
    character(len=*), intent(in) :: text
    character(len=*) avar
    logical, intent(in) :: upper
    integer len, ios
    character*80 an
    call cnprint(trim(text)//"  "//trim(avar)//" ?  ")
    read(*,'(a)',iostat=ios)  an
    len=len_trim(an)
    if(len>0) then
       if(upper) call MakeUpperCase(an)
       avar=an
    endif
    return
  end subroutine ask_text

  subroutine askc(avar)
    implicit none
    character(len=*) avar
    integer ios,len
    character*80 an
    read(*,'(a)',iostat=ios) an
    len=len_trim(an)
    if(len>0) then
       call MakeUpperCase(an)
       avar=an
    endif
    return
  end subroutine askc

  !******************************************************************************
  !                            GetListedNumbers
  !******************************************************************************
  logical function GetListedNumbers(list_string, list, n, nmax, &
       listval_min, listval_max, sorted)
    !-------------------------------------------------------------------------
    ! input:
    !     list_string    string from which listed numbers shall be extracted
    !     nmax           length of array list in calling program
    !     listval_min    allowed minumum of values in list
    !     listval_max    allowed maximum of vulues in list
    !     sorted         specifies if values in list are sorted (default=.true. 
    ! output:
    !     list           integer array with resulting list
    !     n              number of elements in list
    !
    ! Rhe function returns .true. on success and .false. on error
    !
    ! Accepted formats of list string: 
    !   1-11,20    without spaces
    !   -11,20     get all values from listval_min to -11 and 20
    !   11-,20     get all values from 11 to listval_max and 20
    !
    !list is sorted and and list elements occurring more than once are removed
    !-------------------------------------------------------------------------
    implicit none
    character(len=*), intent(in) :: list_string
    integer, intent(in) :: nmax, listval_min, listval_max
    integer, intent(out) :: list(nmax), n
    logical, optional, intent(in) :: sorted
    integer i, k, l, dash_pos, kfrom ,kto, list_min, kmin,&
         listval_from, listval_to

    integer sorted_list(nmax)     ! for sorting
    logical sorting
    logical mask(nmax)            ! for sorting
    character(len=len_trim(list_string)) str
    sorting=.true.
    if(present(sorted)) sorting=sorted

    GetListedNumbers=.false.
    if(len_trim(list_string).eq.0) return


    if(list_string.eq.'*') then
       GetListedNumbers=.true.
       k=listval_min
       do i=1,nmax
          list(i)=k
          if(k.eq.listval_max) then
             n=k
             print *,' all elements selected. List length is ',n
             return
          endif
          k=k+1
       end do
    endif

    i=1
    kfrom=1
    do 
       str=list_string(kfrom:)
       kto=index(str,',')
       if(kto.eq.0) kto=len_trim(str) ! no further comma present
       if(kto.eq.0) exit     

       dash_pos=index(str(1:kto),'-')
       if(dash_pos.gt.0) then
          if(dash_pos.eq.1) then      
             read(str(2:),*,end=999,err=999) listval_to
             listval_from=listval_min
          elseif(dash_pos.eq.kto) then      !dash at end of last token
             read(str(1:kto-1),*,end=999,err=999) listval_from
             listval_to=listval_max 
          elseif(dash_pos.eq.kto-1) then    
             if(str(kto:kto).eq.',') then
                read(list_string(1:kto-2),*,end=999,err=999) listval_from
                listval_to=listval_max                
             else
                read(list_string(1:dash_pos-1),*,err=999,end=999) listval_from
                read(str(dash_pos+1:),*,err=999,end=999) listval_to
             endif
          else
             read(list_string(1:dash_pos-1),*,err=999,end=999) listval_from
             read(str(dash_pos+1:),*,err=999,end=999) listval_to
          end if

          do 
             list(i)=listval_from
             i=i+1
             if(i.gt.nmax) then
                print *,' Too many elements in list'
                return
             endif
             if(listval_from.eq.listval_to) exit
             listval_from=listval_from+1
          end do

       else
          read(str,*,end=999, err=999) list(i)
          i=i+1
          if(i.gt.nmax) then
             print *,' Too many elements in list'
             return
          endif
       endif
       kfrom=kfrom+kto
    end do

    n=i-1
    GetListedNumbers=.true.
    do i=1,n
       if(list(i).gt. listval_max .or. list(i) .lt. listval_min) then
          print *,' Some value in list of Numbers is out of range'
          GetListedNumbers=.false.
          exit
       end if
    end do

    !check for double occurence of list elements and remove

    i=1
    do while(i.lt.n)
       k=i+1
       do while(k.le.n)
          if(list(i).eq.list(k)) then
             n=n-1
             do l=i,n
                list(l)=list(l+1)
             end do
          else
             k=k+1
          endif
       end do
       i=i+1
    end do

    if(.not.sorting) return

    !sort in ascending order


    mask(1:n)=.true.


    do i=1,n
       list_min=2**30
       kmin=0
       do k=1,n
          if(mask(k).and.(list(k).le.list_min)) then
             kmin=k
             list_min=list(k)
          endif
       end do
       if(kmin.gt.0) then
          mask(kmin)=.false.
          sorted_list(i)=list(kmin)
       endif
    end do
    do i=1,n
       list(i)=sorted_list(i)
    end do
    return
999 print *,' Error reading list of numbers'
    return
  end function GetListedNumbers


  !**********************************************************************
  !*                  remove_multiple_spaces (replace them by single)
  !**********************************************************************
  subroutine remove_multiple_spaces(str)
    character*(*) str
    integer slen
    integer i,k
    slen=len(str)
    k=1
    do i=1,slen-1
       if((str(i:i).eq.' ').and.str(i+1:i+1).eq.' ') cycle
       str(k:k)=str(i:i)
       k=k+1
    end do
    do i=k,slen
       str(i:i)=char(32)
    end do

    return
  end subroutine remove_multiple_spaces

  !********************************************************************
  !                   compact_string   (remove spaces)
  !********************************************************************
  subroutine compact_string(str)
    character*(*) str
    integer i,k,slen
    slen=len(str)
    k=1
    do i=1,slen
       if(str(i:i).eq.' ') cycle
       str(k:k)=str(i:i)
       k=k+1
    end do
    do i=k,slen
       str(i:i)=char(32)
    end do
    return
  end subroutine compact_string

  !**********************************************************************
  !                    MakeUpperCase
  !**********************************************************************      
  subroutine MakeUpperCase(str)
    implicit none
    character*(*) str
    integer i,k,n
    k=len_trim(str)
    do i=1,k                 ! convert command to upper case
       n=ichar(str(i:i))
       if(n.ge.97.and.n.le.122) str(i:i)=char(n-32)
    end do
    return
  end subroutine MakeUpperCase
  !**********************************************************************
  !                     UpperCase
  !**********************************************************************
  function UpperCase(str)
    !trim string and make it upper case
    implicit none
    character(Len=*), intent(in) :: str
    character(len=len_trim(str)) :: UpperCase
    integer i,n,k

    k=len_trim(str)
    UpperCase=str(1:k)
    do i=1,k
       n=ichar(str(i:i))
       if(n.ge.97.and.n.le.122) UpperCase(i:i)=char(n-32)
    end do
    return
  end function UpperCase

  !**********************************************************************
  !                     count_tokens
  !**********************************************************************
  subroutine count_tokens(str,count)
    !count space separated tokens
    implicit none
    character*(*), intent(in) :: str
    integer, intent(out) :: count
    integer len,i,ifrom
    count=0
    len=len_trim(str)
    if(len.eq.0) return
    ifrom=1
    do i=1,len
       if(str(i:i).eq.' ') cycle
       ifrom=i
       exit
    end do
    count=1
    do i=ifrom, len-1
       if(str(i:i).eq.' '.and. str(i+1:i+1).ne.' ') count=count+1 
    end do
    return
  end subroutine count_tokens

  !*******************************************************************
  !  iindex is case insensitive version of intrinsic function index
  !*******************************************************************
  integer function iindex(str, substr)
    ! find position of trimmed string substr in str
    implicit none
    character(len=*) str, substr
    iindex=index(UpperCase(str), UpperCase(substr))
    return
  end function iindex

end module utils
