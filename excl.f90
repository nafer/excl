module yellin_links
  integer, private, parameter :: n=7
  character(len=32),private :: &
       yeldir='/opt/Programs/excl/Yellin/',&
       fn(n)=(/&
       'CLtableNew.in ',&
       'CMaxf.in      ',&
       'CMaxfLow.in   ',&
       'CMaxflowNew.in',&
       'ymintable.in  ',&
       'y_vs_CLf.in   ',&
       'CLtable.txt   '/)

contains
  subroutine makeYellinLinks()
    implicit none
    integer :: i, ios, status

    do i=1,n
       open(unit=1, file=fn(i), status='old', action='read',iostat=ios)
       if(ios.ne.0) then
          call symlnk(trim(yeldir)//trim(fn(i)), trim(fn(i)), status)
       endif
       close(unit=1)
    end do
    return
  end subroutine makeYellinLinks

  subroutine removeYellinLinks()
    do i=1,n
       if(unlink(trim(fn(i))) .ne.0 ) then
          print *,'Removal of '//trim(fn(i))//' failed'
       endif
    end do
  end subroutine removeYellinLinks

end module yellin_links



program excl
  !*******************************************************************
  !purpose: calculate exclusion plot at 90% cl using Yellins optimal
  !interval method: Phys. Rev. D 66, 032005 (2002)
  !********************************************************************
  use SomeData
  use parameters, only : get_yield_limit, get_probab, read_start
  use yield_range, only : fraction 
  use eff_tables, only : Eremove_from, Eremove_to, remove_bins
  use target_type, only : get_target_name, get_upper_band, get_lower_band
  use utils, only : PrintHelp, aska, askf, aski
  use yellin_links
  implicit none   
  integer EndPoints   ! lower and upper index of the optimum interval 
  real Exclude_low
  Common/UpperLimCom/EndPoints(2),Exclude_low(2)
  real*8 :: sig_c0,&           ! calculates sigma from maximum gap
       QF, get_integrated_spectrum
  external sig_c0, QF, get_integrated_spectrum


  real*8 ::&
       exposure, beta,&

       alpha,&                        ! expectet wimp counts in interval 
       eu(0:maxnd+1), eo(0:maxnd+1),& ! most effective energy range with index events inside
       MT(maxdets), md,& 

       eta_t(0:maxnd*maxdets+1),&     ! transformed event energies (make distribution flat)
       elow, ehigh,&
       xu, xo, zstep


  ! ******** single precision stuff to feed to Yellin's routine *****************************
  real, allocatable :: FC(:),FCB(:) ! transformed event energies (make distribution flat)
  real :: sigmin, mfrom, mto
  real, parameter :: &
       muB=0.,&             ! don't subtract any background
       CL=0.9,&             ! confidence level
       sigma0=1.            ! unity cross section [pb] for which DM spectrum is calculatd
  integer:: iflag,&         ! use new table (0=old, 2=same ?)
       ifl

  integer :: idpair(maxdets*maxnd)

  integer :: i, ifrom, ito, k, kfrom, kto, nr, alloc_status, ios,&
       step, steps, nd_t, nd_t_from

  logical :: quiet, did_replace, export_eta=.false.
  character*80  taskfile,  data_file(maxdets), exposure_file(maxdets), start_file(maxdets),&
       cut_efficiency_file(maxdets), trigger_efficiency_file(maxdets), outfile, temp_str,&
       data_file_replacement
  character  maximum_gap*1 /'N'/ 

  real UpperLim           ! Yellin's limit routine
  external UpperLim

 
  if(iargc().eq.0) then
     call PrintHelp("help.hlp")
     stop
  endif

  quiet=.false.

  call parse_command_line()
  
  call makeYellinLinks()
  
  call read_task_file()
  
  call read_exposure_files()
  
  call read_cut_and_trigger_efficiency_files()

  ! read 2d data for CaWO4 and extract events from 2d acceptance region
  ! or read event energies and extract events from accepted energy range
  ! start file specifies target type of non scintillating targets
  call read_data_files(data_file, start_file, quiet)
 
  if(.not.quiet) then
     call askf("Lowest WIMP MASS:",mfrom)
     call askf("Highest WIMP Mass:",mto)
     call aski("Number of steps:",steps)
  endif

  md=mfrom
  zstep=exp(log(mto/mfrom)/(steps-1))

  ! when data_file has appended number string then append same number 
  ! string to output file

  ifrom=index(data_file(1),'_',back=.true.)
  outfile='limit.xy'
  if(ifrom.gt.0) then
     ito=index(data_file(1),'.',back=.true.)
     if(ito.gt.0) then
        read(data_file(1)(ifrom+1:ito-1),*,iostat=ios) i
        if(ios.eq.0) outfile='limit'//data_file(1)(ifrom:ito)//'xy'
     endif
  endif
  print *,'excl: will write limits to file '//trim(outfile)//',  data file: '//data_file(1)
  
  open(unit=2,file=outfile,status='unknown')
  if(.not.quiet) then
     print *,' You can choose between Yellins maximum gap or maximum interval methods'
     call aska(' Maximum gap method :',maximum_gap)
  endif
  if(maximum_gap.ne.'Y') then
     write(2,*)"# Yellin's optimal interval method"
     Iflag=1 ! use new Yellin tables
     Call UseNewConfL(Iflag.eq.1)
     Call UseNewCMax(Iflag.eq.1)
  else
     write(2,*)"# Yellin's maximum gap method"
  endif
  write(2,*)'# WIMP mass [GeV]'
  write(2,*)'# WIMP-nucleon cross section [pb]'
  write(2,*) '#',steps, 1, 0, 0., 3, 2

  !************ run exclusion algorithm on range of WIMP masses ***************************
  md=mfrom
  do step=1,steps            ! loop over WIMP masses
     beta=0.
     beta_t=0.   ! expected events of all detectors for 1pb cross section
     exposure=0. ! total exposure  
     nd_t=0      ! number of observed events in all detectors
     do k=1,numdets
        if(read_start(start_file(k),quiet).eq.0) then
           print *,'Error reading start file ',trim(start_file(k))
           stop
        endif

        if(step.eq.1 .and. .not.quiet) then
           print '(a,i3,a)','Detector nr.',k,', Target: ',trim(get_target_name())
        endif

        !* transform recoil energies of detector to Yellin's eta
        !* in which recoil spektrum is flat.
        !* Sum up eta spectra of all detectors

        ! requires that event energies are sorted
        nd_t_from=nd_t
        do i=1,nd(k)
           nd_t = nd_t + 1
           ehigh=energy(i,k)
           idpair(nd_t)=(k-1)*maxnd + i-1  ! k=idpair/maxnd, i=mod(idpair,maxnd)

           if(i.eq.1) then
              elow=efrom(k)
              eta_t(nd_t)=0.   
           else
              elow=energy(i-1,k)
              eta_t(nd_t)=eta_t(nd_t-1)
           endif

           do nr=1,remove_bins(k)
              if(elow.le.Eremove_from(nr,k) .and. ehigh.ge.Eremove_to(nr,k)) then
                 eta_t(nd_t)= eta_t(nd_t) + &
                      get_integrated_spectrum(md, elow,Eremove_from(nr,k), k)*MT(k)
                 elow=Eremove_to(nr,k)
              endif
           end do
           eta_t(nd_t)=eta_t(nd_t)+get_integrated_spectrum(md,elow ,ehigh, k)*MT(k)
        end do

        exposure=exposure+Mt(k)

        !beta: expected events in whole experimental range of this detector 
        !for given exposure and unity cross section (1pb)

        ehigh=eto(k)
        if(nd(k).gt.0) then
           elow=energy(nd(k),k)
           beta=eta_t(nd_t)
        else
           elow=efrom(k)
           beta=0.
        endif
        do nr=1,remove_bins(k)
           if(elow.le.Eremove_from(nr,k) .and. ehigh.ge.Eremove_to(nr,k)) then
              beta=beta+get_integrated_spectrum(md, elow, Eremove_from(nr,k), k)*Mt(k)
              elow=Eremove_to(nr,k)
           endif
        end do
        beta=beta+get_integrated_spectrum(md, elow, ehigh, k)*Mt(k)

        if(beta.le.1.d-36) then
           print '(a,f7.1,a)',' MX: ',sngl(md),', '//trim(data_file(k))
           Print *,' No events expected in experimental range:',sngl(beta)
           print *,' detector will not be included'
           cycle
        endif
        beta_t=beta_t+beta

        nd_t=nd_t_from
        do i=1,nd(k)
           nd_t = nd_t + 1
           eta_t(nd_t)=eta_t(nd_t)/beta
        end do
     end do  ! k=1,numdets

     if(beta.le.1.d-36) then
        print '(a,f7.1,a)',' MX: ',sngl(md),', '//trim(data_file(k))
        Print *,' No events expected in experimental range.'
        print *,' Impossible to calculate limit'
        sigmin=1.e36
     else

        ! add experimental range as first and last transformed energy
        eta_t(0)=0.d0       
        eta_t(nd_t+1)=1.d0  
        call sort2i(nd_t, eta_t(1),idpair)


        if(.not.allocated(fc)) then
           allocate(FC(0:nd_t+1),FCB(0:nd_t+1), stat=alloc_status)
           if(alloc_status.ne.0) then
              print *,'Error allocating FC and FCB'
              stop
           endif
        endif

        do i=0,nd_t+1
           FC(i)=sngl(eta_t(i))  ! avoid problems with single and double versions of libnr
        end do

        if(export_eta) then
           open(unit=1, file="etas.xy")
           do i=0, nd_t+1
              write(1,*) eta_t(i)
           end do
           close(unit=1)
           print *
           print *,'Yellin spectrum exported to eta.xy'
           print *
           export_eta=.false.  ! export only for lowest WIMP mass
        endif


        !***** find  largest interval in eta with nevent events inide ************************* 
        sigmin=1.e36
        if(maximum_gap.eq.'Y') then
           alpha_max(0)=0.
           do i=0,nd_t
              xu=eta_t(i)
              xo=eta_t(i+1)
              alpha=(xo-xu)*beta_t ! expected counts in all detectors for 1pb  in interval
              if(alpha.gt.alpha_max(0)) then
                 alpha_max(0)=alpha
                 eu(0)=xu
                 eo(0)=xo
              endif
           end do
           if(alpha_max(0).eq.0.) then
              print *,' alpha_max(0)=0, mass dropped:',md
              goto 20
           endif
           sigmin=sngl(sig_c0())
        else
           ifl=1          ! fmin=0 in Yellings ExampleNew.f
           sigmin=sngl(sigma0/beta_t)*UpperLim(CL,ifl,nd_t,FC,muB,FCB,iflag)
           if(iflag.gt.1) then
              print '(a,g12.2,a,i5)','WIMP mass: ',md,', UpperLim returned iflag: ',iflag
              call print_yellin_error_codes()
           endif
        endif
     endif
     write(2,*) md,sigmin
     if(.not.quiet) then
        print '(a,2g10.4)',' md, sigma:  ',md,sigmin
        print '(a,g10.4)',' expected counts: ',beta_t*sigmin/sigma0
     endif

     if(maximum_gap.ne.'Y') then
        kfrom=-1
        kto=  -1
        ifrom=0
        ito=0
        if(Endpoints(1)>0) then
           kfrom=idpair(Endpoints(1))/maxnd+1
           ifrom=mod(idpair(Endpoints(1)),maxnd)+1
        endif
        if(Endpoints(2)<nd_t+1) then
           kto=idpair(Endpoints(2))/maxnd+1
           ito=mod(idpair(Endpoints(2)),maxnd)+1
        endif

        if(kfrom<0 .and. kto>0) then 
           kfrom=kto
           eu(0)=efrom(kto)
           eo(0)=energy(ito,kto)
        else if(kfrom>0 .and. kto<0) then
           kto=kfrom
           eu(0)=energy(ifrom,kfrom)
           eo(0)=eto(kfrom)
        else if(kfrom<0 .and. kto<0) then
           eu(0)=minval(efrom(1:numdets))
           eo(0)=maxval(eto(1:numdets))
        else
           eu(0)=energy(ifrom,kfrom)
           eo(0)=energy(ito,kto)
        endif

        if(.not.quiet) then
           if(kfrom>0) then
              print '(2(a,g10.4),2(a,i2),a/)',&
                   ' Optimum interval: ', eu(0),' to ',eo(0),&
                   ', (detector', kfrom,', ', kto, ')'
           else
              print '(a,2g10.2,/)','optimum_interval:',eu(0), eo(0)
           endif
        endif
     else
        if(.not.quiet) print '(a,2g10.2,/)','largest empty interval:',&
             eu(0), eo(0)
     endif
20   md=md*zstep
  end do
  
  if(.not.quiet) then
     print *,' exclusion plot written to file '//trim(outfile)
     print *,' Total exposure: ',exposure
  endif
  call removeYellinLinks()
contains

 
  !**********************************************************************
  !*                         print_yellin_error_codes
  !**********************************************************************
  subroutine print_yellin_error_codes()
    if(iand(iflag,2).gt.0) then
       print *,"More than 10 iterations needed, but not obtained.  This may be serious."
    endif
    if(iand(iflag,4).gt.0) then
       print *,"y_vs_CLf returned status 1 at some time (extrapolated from f0=0.01)"
    endif
    if(iand(iflag,8).gt.0) then
       print *,"y_vs_CLf returned status 2 at some time (extrapolated from f0=1)"
    endif
    if(iand(iflag,16).gt.0) then
       print *,"The optimum interval had status 1."
    endif
    if(iand(iflag,32).gt.0) then
       print *,"The optimum interval had status 2."
    endif
    if(iand(iflag,64).gt.0) then
       print *,"Failure to solve CMax = CMaxbar."
    endif
    if(.not.did_replace) then
       if(iand(iflag,128).eq.0) then
          print *,"Couldn't solve CMax=CMaxbar because upperlim wants to be <0."
       endif
       if(iand(iflag,256).gt.0) then
          print *,"More than one solution.  The other two are in Exclude_low, which gives"
          print *,"the excluded range below the absolute upper limit."
       endif
    endif
    return
  end subroutine print_yellin_error_codes

  !**********************************************************************
  !                          excl::read_task_file
  !**********************************************************************
  subroutine read_task_file()
    ! define:
    ! number of detectors or periods to deal with
    ! data files  for each detector
    ! start file with fit parameters determined by yield_fit for each detector 
    ! file names with ratio data for each detector
    use utils, only : cprint, ceprint
    implicit none
    integer ios
    character*80 temp_str

    open(unit=1,file=taskfile,status='old',iostat=ios)
    if(ios.ne.0) then
       call ceprint("#Error opening task file "//trim(taskfile)//"#")
       stop
    endif
    !************** number of detectors ************************************
    do
       read(1,'(a)',end=5,err=5) temp_str
       if(len_trim(temp_str).gt.0) exit
    end do
    read(temp_str,*,end=5,err=5) numdets         
    if(numdets.gt.maxdets) then
       call ceprint("#Too many files, recompile with larger maxdets#")
       stop
    endif
    do i=1,numdets
       !**************  name of start files *******************************
       do
          read(1,'(a)',end=5,err=5) temp_str
          if(len_trim(temp_str).gt.0) exit
       end do
       read(temp_str,'(A)',end=5,err=5) start_file(i) 

       !**************** name of data files ******************************
       do
          read(1,'(a)',end=5,err=5) temp_str
          if(len_trim(temp_str).gt.0) exit 
       enddo
       if(.not.did_replace) read(temp_str,'(A)',end=5,err=5) data_file(i)  
!!$       print *,'Data file name '//trim(data_file(i))//' read'

       !**** files with exposure, experimental range, phonon resolution ****
       do
          read(1,'(a)',end=5,err=5) temp_str
          if(len_trim(temp_str).gt.0) exit 
       enddo
       read(temp_str,'(A)',end=5,err=5)  exposure_file(i)
!!$       print *,'Exposure file name '//trim(exposure_file(i))//' read'


       !***** files with energy dependent cut efficiencies *****************
       do
          read(1,'(a)',iostat=ios) temp_str
          if(ios.ne.0) then
             print *,'No file name for cut efficiencies in task file'
             goto 4
          endif
          if(len_trim(temp_str).gt.0 .and. index(temp_str,'#').eq.0) exit 
       enddo
       read(temp_str,'(A)',end=5,err=5)  temp_str
       if(index(temp_str,'$').gt.0) cycle
       cut_efficiency_file(i)=temp_str
!!$       print *,'file name with cut efficiencies '//trim(exposure_file(i))//' read'

       !***** files with energy dependent trigger efficiencies *************
       do
          read(1,'(a)',iostat=ios) temp_str
          if(ios.ne.0) then
             print *,'No file name for trigger efficiencies in task file'
             goto 4
          endif
          if(len_trim(temp_str).gt.0 .and. index(temp_str,'#').eq.0) exit 
       enddo
       read(temp_str,'(A)',end=5,err=5)  temp_str
       if(index(temp_str,'$').gt.0) cycle
       trigger_efficiency_file(i)=temp_str


!!$       print *,'file name with trigger efficiencies '//trim(exposure_file(i))//' read'

    end do
4   close(unit=1)
    goto 6
5   print *,'Error occurred while reading '//taskfile(1:len_trim(taskfile))
    stop
6   return
  end subroutine read_task_file

 
  !**********************************************************************
  !        excl::read_cut_and_trigger_efficiency_files
  !**********************************************************************
  subroutine read_cut_and_trigger_efficiency_files()
    use eff_tables
    implicit none
    integer i,k,ios
    character*80 temp_str
    do k=1,numdets
       open(unit=1,file=cut_efficiency_file(k),status='old',iostat=ios)
       if(ios.ne.0) then
          if(.not. quiet) then
             print *,'Error opening file with cut efficiencies '&
                  //trim(cut_efficiency_file(k))
             print *,'Will assume all cut efficiencies are 1'
          endif
          cut_bins(k)=0
       else
          read(1,'(a)',err=990) ! skip plotxy header
          read(1,'(a)',err=990) ! skip plotxy header
          read(1,'(a)',err=990) ! skip plotxy header
          read(1,'(a)',err=990) temp_str
          read(temp_str(index(temp_str,'#')+1:),*,err=990) cut_bins(k)
          if(cut_bins(k).gt.max_cut_bins) then
             print *,'Number of elements in file '//trim(cut_efficiency_file(k))//&
                  ' exceeds limit'
             print *,'Increase size of max_cut_bins at least to ',cut_bins(k)
             stop
          endif
          do i=1,cut_bins(k)
             read(1,*,err=990) ecut(i,k), cut_eff(i,k)
          end do
          read(1,'(a)',iostat=ios) temp_str
          if(index(temp_str,'+').gt.0) then
             read(1,*,err=990) remove_bins(k)
             print *,'Number of energy ranges to remove: ',remove_bins(k)
             if(remove_bins(k).gt.max_remove_bins) then
                print *,'Too many gamma lines to remove'
                print *,'Increase max_remove_bins and remake excl'
             endif
             do i=1,remove_bins(k)
                read(1,*,err=990) Eremove_from(i,k), Eremove_to(i,k)
             end do
          endif
          close(unit=1)
          call sort2(cut_bins(k),ecut(1,k),cut_eff(1,k))
       endif


       open(unit=1,file=trigger_efficiency_file(k),status='old',iostat=ios)
       if(ios.ne.0) then
!!$             print *,'Error opening file '//trim(trigger_efficiency_file(k))
!!$             print *,'Will assume fully efficient trigger'
          trig_bins(k)=0
       else
          read(1,'(a)',err=999) ! skip plotxy header
          read(1,'(a)',err=999) ! skip plotxy header
          read(1,'(a)',err=999) ! skip plotxy header
          read(1,'(a)',err=999) temp_str
          read(temp_str(index(temp_str,'#')+1:),*,err=999) trig_bins(k)
          if(trig_bins(k).gt.max_trig_bins) then
             print *,'Number of elements in file '//trim(trigger_efficiency_file(k))//&
                  ' exceeds limit'
             print *,'Increase size of max_trig_bins at least to ',trig_bins(k)
             stop
          endif
          do i=1,trig_bins(k)
             read(1,*,err=999) etrig(i,k), trig_eff(i,k)
          end do
          close(unit=1)
          call sort2(trig_bins(k),etrig(1,k),trig_eff(1,k))
       endif
    end do
    return
990 print *,'Error occurred while reading cut efficiencies from '//&
         trim(cut_efficiency_file(k))
    close(unit=1)
    stop
999 print *,'Error occurred while reading trigger efficiencies from '//&
         trigger_efficiency_file(k)
    close(unit=1)
    stop
  end subroutine read_cut_and_trigger_efficiency_files

  !**********************************************************************
  !                     excl::read_exposure_files
  !**********************************************************************
  subroutine read_exposure_files()
    use utils, only: aska
    implicit none
    integer k, n
    real elow, ehigh
    character*1 ans
    !****************************************************************
    ! 
    ! read exposure, 
    ! read experimental energy range
    ! ask experimental energy range common for all detectors (when
    ! not in quiet mode)
    ! read target type
    !****************************************************************

    do k=1,numdets
       n=len_trim(exposure_file(k))
       open(unit=1,file=exposure_file(k),iostat=ios)
       if(ios.ne.0) then
          print *,'Error opening file exposure file '//exposure_file(k)(1:n)
          stop
       endif

       read (1,*,iostat=ios) MT(k)              ! exposure in kg-days
       if(ios.ne.0) then
          print *,'Error reading exposure in file '//exposure_file(k)(1:n)
          stop
       endif

       read(1,*,iostat=ios) efrom(k), eto(k)    ! useful energy range
       if(ios.ne.0) then
          print *,'Error reading usful energy range from '//exposure_file(k)(1:n)
          stop
       endif
       close(unit=1)
    end do

    ans='N'
    if(.not.quiet) call aska('use same useful range for all detectors: ',ans)
    if(ans.eq.'Y') then
       elow=sngl(efrom(1))
       ehigh=sngl(eto(1))
       call askf('Lower limit [keV]:',elow)
       call askf('upper limit [keV]:',ehigh)
       do k=1,numdets
          efrom(k)=elow
          eto(k)=ehigh
       end do
    endif
    return
  end subroutine read_exposure_files


  !**********************************************************************
  !                   excl::parse_command_line
  !**********************************************************************
  subroutine parse_command_line()
    use utils, only : PrintHelp
    implicit none
    data_file_replacement=''
    mfrom=-1.
    mto=-1.
    steps=-1
    did_replace=.false. ! ratio file was replaced via command line argument
    i=0
    do
       i=i+1
       if(i.gt.iargc()) exit
       call getarg(i,temp_str)
       if(index(temp_str,'-H')>0 .or.index(temp_str,'-h')>0) then
          call PrintHelp("help.hlp")
          stop
       endif
       
       if(index(temp_str,'-task' )    > 0) then
          call PrintHelp("task.hlp")
          stop
       endif
       if(index(temp_str,'-start')    > 0) then
          call PrintHelp("start.hlp")
          stop
       endif
       if(index(temp_str,'-data' )    > 0) then
          call PrintHelp("data.hlp")
          stop
       endif
       if(index(temp_str,'-exposure') > 0) then
          call PrintHelp("exposure.hlp")
          stop
       endif
       if(index(temp_str,'-cut' )     > 0) then
          call PrintHelp("cut.hlp")
          stop
       endif

       if(i.eq.1) then
          taskfile=temp_str
          cycle
       endif
       if(index(temp_str,"-infile").gt.0) then
          !this is some option for calling excl in yield_fit
          i=i+1
          if(i.gt.iargc()) stop ' name of ratio file replacement missing'
          call getarg(i,data_file_replacement)
          if(len_trim(data_file_replacement).gt.0) then
             data_file(1)=trim(data_file_replacement)
             did_replace=.true.
          endif
          export_eta=.false.
       elseif(index(temp_str,"-quiet").gt.0) then
          quiet=.true.
       elseif(index(temp_str,"-fu").gt.0) then
          i=i+1
          if(i.gt.iargc()) stop ' Value for -fu option missing'
          call getarg(i,temp_str)
          read(temp_str,*,iostat=ios) fraction(1)
          if(ios.ne.0) stop 'Error reading value for -fu option'
          if(.not.quiet) print *,'Fraction of upper band to include set to: ',fraction(1)
       elseif(index(temp_str,"-fl").gt.0) then
          i=i+1
          if(i.gt.iargc()) stop 'Value for -fl option missing'
          call getarg(i,temp_str)
          read(temp_str,*,iostat=ios) fraction(2)
          if(ios.ne.0) stop 'Error reading value for -fl option'
          if(.not.quiet) print *,'Fraction of lower band to include set to: ',fraction(2)
       elseif(index(temp_str,"-mfrom").gt.0) then
          i=i+1
          if(i.gt.iargc()) stop ' Value for -mfrom option missing'
          call getarg(i,temp_str)
          read(temp_str,*,iostat=ios) mfrom
          if(ios.ne.0) stop 'Error reading value for the -mfrom option'
          if(.not.quiet) print *,'Lower mass limit for limit calculation set to :',mfrom     
       elseif(index(temp_str,"-mto").gt.0) then
          i=i+1
          if(i.gt.iargc()) stop ' Value for -mto option missing'
          call getarg(i,temp_str)
          read(temp_str,*,iostat=ios) mto
          if(ios.ne.0) stop 'Error reading value for the -mto option'
          if(.not.quiet) print *,'Upper mass limit for limit calculation set to:',mto
       elseif(index(temp_str,"-steps").gt.0) then
          i=i+1
          if(i.gt.iargc()) stop ' Value for -steps option missing'
          call getarg(i,temp_str)
          read(temp_str,*,iostat=ios) steps
          if(ios.ne.0) stop 'Error reading value for the -steps option'
          if(.not.quiet) print *,'Number of mass steps per decade set to:',steps
       endif
    enddo
    if(mfrom.lt.0.) mfrom=0.1
    if(mto.lt.0.) mto=100.
    if(steps.lt.0) steps=30
  end subroutine parse_command_line
end program



!******************************************************************************
!                           func_c0
!******************************************************************************
real*8 function func_c0(sigma)

  !func_c0 is 0 for the cross section (pb) which excludes maximum gap at 90% CL
  !input via common block:
  !alpha_max: largest number of wimps expected for unity cross section in any interva with
  !           nevents inside
  !beta_t     : number of wimps expected in whole experimental range [0,1] for all detectors
  !n_inside   : number of events in interval of consideration
  use SomeData, only : beta_t, alpha_max
  implicit none
  real*8 sigma

  real*8 x,mu, C0
  external C0

  x=sigma*alpha_max(0)
  mu=sigma*beta_t
  func_c0=C0(x,mu)-0.9d0
  return
end function func_c0

!**********************************************************************
!                         sig_c0
!**********************************************************************
real*8 function sig_c0()
  !-----------------------------------------
  !find singma for which func_c0(sigma)=0
  !-----------------------------------------
  use SomeData, only : alpha_max
  implicit none

  real*8 zbrent, func_c0
  external func_c0, zbrent
  real*8 sig1, sig2, tol
  logical success

  if(alpha_max(0).eq.0) then
     print *,' alpha_max(0)=0'
     stop
  endif

  sig1=1./alpha_max(0)
  sig2=3./alpha_max(0)
  call zbrac(func_c0,sig1,sig2,success)
  if(.not.success) then
     print *,' no bracketing sig1, sig found by routine zbrac '
  endif

  tol=(sig1+sig2)*0.5e-6
  sig_c0=zbrent(func_c0,sig1,sig2,tol)
  return
end function sig_c0


!**********************************************************************
!                    read_data_files
!**********************************************************************
subroutine read_data_files(data_file, start_file, quiet)
  !-------------------------------------------------------------
  ! Read start files (start gives information whether light yield
  ! will be available. When available it contains fit parameters of
  ! band fit.
  ! Read data files and extract event energies in acceptance region.
  ! Acceptance region may be just the energy interval when no light
  ! yield is available or an acceptance region in energy-yield plane
  !---------------------------------------------------------------
  use parameters, only : get_yield_limit, read_start
  use target_type
  use SomeData, only : nd, maxnd, numdets, maxdets, energy, efrom, eto
  use yield_range, only : fraction, type
  use utils, only : ceprint, aska
  implicit none
  character(len=*), intent(in) :: data_file(maxdets), start_file(maxdets)
  logical, intent(in) :: quiet
  integer, parameter :: loch=999999
  real, parameter :: floch=1.e36

  integer i, k, ios, nel, n, alloc_status, nav
  real*8   yield_max, yield_min, yield_efrom, yield_eto, ep, yield
  real*8, allocatable :: x(:), y(:)

  character*80 temp_str
  character*1 draw
  draw='N'
  
  do k=1,numdets
     if(read_start(start_file(k),quiet).eq.0) then
        call ceprint('#Error reading start file '//trim(start_file(k))//'#')
        stop
     endif
     ! not compatible with command line options -bu -bl
       if(.not.quiet) then
        if(k.eq.1 .and. use_light()) then
           call aska('Draw data (Y) and also (E)xport acceptance region:',draw)
        endif
     endif
     print *,'excl: Reading data file '//trim(data_file(k))
     open(unit=1,file=data_file(k),status='old',iostat=ios)
     if(ios.ne.0) then
        call ceprint('#Error opening energy, yield data file '//trim(data_file(k))//'#')
        stop
     endif
     read(1,'(a)',iostat=ios)
     read(1,'(a)',iostat=ios) 
     read(1,'(a)',iostat=ios) 
     read(1,'(a)',iostat=ios) temp_str
     read(temp_str(index(temp_str,'#')+1:),*,iostat=ios) nel
     if(allocated(x)) then
        if(size(x).lt.nel) then
           deallocate(x,y)
           allocate(x(nel),y(nel), stat=alloc_status)
           if(alloc_status.ne.0) then
              stop 'Error while allocating memory in read_data_file'
           endif
        endif
     else
        allocate(x(nel),y(nel),stat=alloc_status)
        if(alloc_status.ne.0) then
           stop 'Error while allocating memory in read_data_file'
        endif
     endif

     nd(k)=0
     n=0
     do i=1,nel
        if(use_light()) then
           read(1,*,iostat=ios) ep,yield
        else
           read(1,*,iostat=ios) ep
        endif
        if(ios.ne.0) then
           print *,'Error reading '//trim(data_file(k))//' line nr.: ',i
           stop
        endif
        if(ep.lt. efrom(k)) cycle
        if(ep.gt.eto(k))    cycle
        if(skip_event(k))   cycle
        n=n+1
        x(n)=ep          
        if(use_light()) then
           y(n)=yield

           yield_max=get_yield_limit(ep,type(1),fraction(1))   ! oxygen
           if(yield.gt.yield_max) cycle

           if(fraction(2).gt.1.e-10) then
              yield_min=get_yield_limit(ep,type(2),fraction(2)) ! tungsten
           else
              yield_min=-100.
           endif
           if(yield.le.yield_min) cycle
        endif

        nd(k)=nd(k)+1
        if(nd(k).gt.maxnd) then
           print *,' Too many accepted events in file '//trim(data_file(k))
           print *,' recompile with larger maxnd'
           stop
        endif
        energy(nd(k),k)=ep ! event energies in experimental range
     end do
     close(unit=1)

     nel=n
     call sort(nd(k),energy(1,k))
#ifdef _GRAPHX_AVAILABLE_
     if(use_light()) then
        if(draw.ne.'Y') then
           print '(a,i3,a,i10)','Detector number ',k,', accepted events: ',nd(k)
           print '(8f10.4)',energy(1:nd(k),k)
        endif
        if(draw.eq.'Y'.or.draw.eq.'E') call draw_energy_yield_data(k)
     endif
#endif
  end do
  return
contains
  logical function skip_event(detnum)
    use eff_tables, only : remove_bins, Eremove_from, Eremove_to
    implicit none
    integer, intent(in) :: detnum
    integer nr
    skip_event=.false.
    do nr=1,remove_bins(detnum)
       if(ep.ge.Eremove_from(nr,detnum) .and. ep .le. Eremove_to(nr,detnum)) then
          skip_event=.true.
          exit
       endif
    enddo
  end function skip_event

#ifdef _GRAPHX_AVAILABLE_
  subroutine draw_energy_yield_data(detnum)
    use yield_range, only : fraction, type
    implicit none
    integer, intent(in) :: detnum
    real*8 :: xmin,xmax, ymin=-0.4, ymax=1.5
    integer nf
    xmin=efrom(detnum)
    xmax=eto(detnum)
    print *,' Detector number ',detnum,', accepted events: ',nd(detnum)
    if(nd(detnum).gt.0) then
       print *,repeat("*",5)//' '//trim(data_file(detnum))//' '//repeat("*",5)
       print '(8f10.4)',(energy(i,k),i=1,nd(detnum))
    endif

    call graope
    call graxi1(-4,loch,loch,floch)
    call grayi1(-4,loch,loch,floch)
    call grawnd(sngl(xmin), sngl(xmax), sngl(ymin), sngl(ymax))

    call gracoi(mod(detnum-1,4)+1)
    call graph1d(x, y, nel, 0, 11, 0.15)

    nav=0
    n=min(nel,1000)
    yield_efrom=0.
    yield_eto=0.
    call gracoi(4)
    if(draw.eq.'E') then
       write(temp_str,'(a,i2.2,a)') 'data_with_limit_line_',k,'.txt'
       open(unit=1,file=temp_str)

       write(1,*) 'Accepted events: ',nd(detnum)
       write(1,*) 'Energy [keV]'
       write(1,*) 'Light Yield'
       write(1,*) nel, ', 0, 1, 0.1'
       do i=1,nel
          write(1,*) sngl(x(i)),sngl(y(i))
       end do
       write(1,*) 2*n, 1, 0, 0.1
    endif
    do nf=1,2
       do i=1,n
          x(i)=xmin+(xmax-xmin)*(i-1)/(n-1)
          if(fraction(nf).gt.1.e-30) then
             y(i)=get_yield_limit(x(i),type(nf),fraction(nf))
!!$                   print '(a,i2,2f10.4)','type, E, Q',type,x(i),QF(x(i),type)
          else
             y(i)=-100.
          endif
          if(nf.eq.1) then
             if(x(i).gt.efrom(detnum) .and. yield_efrom.eq.0.) yield_efrom=y(i)
             if(x(i).le.eto(detnum)) yield_eto=y(i)
          endif
       end do
       call graph1d(x,y,n,1,0,0.)
       if(draw.eq.'E') then
          if(nf.eq.1) write(1,'(2g15.5)') (x(i),y(i),i=1,n)
          if(nf.eq.2) write(1,'(2g15.5)') (x(i),y(i),i=n,1,-1)
       endif
    end do
    if(draw.eq.'E') close(unit=1)
    call gracoi(1)
    x(1)=efrom(detnum)
    x(2)=efrom(detnum)
    y(1)=ymin
    y(2)=yield_efrom
    call graph1d(x,y,2,2,0,0.)

    x(1)=eto(detnum)
    x(2)=eto(detnum)
    y(1)=ymin
    y(2)=yield_eto
    call graph1d(x,y,2,2,0,0.)


    call gracls
    return
  end subroutine draw_energy_yield_data
  
  subroutine graph1d(xd, yd, nel, linas, imar, hmar)
  integer nel, linas,imar
  real*8 xd(nel),yd(nel)
  real hmar
  real x(nel), y(nel)
  integer i
  do i=1,nel
     x(i)=sngl(xd(i))
     y(i)=sngl(yd(i))
  end do
  call graph1(x,y,nel,linas,imar,hmar)
  return
end subroutine graph1d
#endif
  
end subroutine read_data_files

 



SUBROUTINE sort2i(n,arr,brr)
  ! same as sort2, however with integer array brr
  implicit none
  integer, parameter :: M=7,NSTACK=50
  INTEGER n
  REAL*8 arr(n)
  integer brr(n)

  INTEGER i,ir,j,jstack,k,l,istack(NSTACK)
  REAL*8 a, temp
  integer b, itemp
  jstack=0
  l=1
  ir=n
1 if(ir-l.lt.M)then
     do j=l+1,ir
        a=arr(j)
        b=brr(j)
        do i=j-1,1,-1
           if(arr(i).le.a)goto 2
           arr(i+1)=arr(i)
           brr(i+1)=brr(i)
        end do
        i=0
2       arr(i+1)=a
        brr(i+1)=b
     end do
     if(jstack.eq.0)return
     ir=istack(jstack)
     l=istack(jstack-1)
     jstack=jstack-2
  else
     k=(l+ir)/2
     temp=arr(k)
     arr(k)=arr(l+1)
     arr(l+1)=temp
     itemp=brr(k)
     brr(k)=brr(l+1)
     brr(l+1)=itemp
     if(arr(l+1).gt.arr(ir))then
        temp=arr(l+1)
        arr(l+1)=arr(ir)
        arr(ir)=temp
        itemp=brr(l+1)
        brr(l+1)=brr(ir)
        brr(ir)=itemp
     endif
     if(arr(l).gt.arr(ir))then
        temp=arr(l)
        arr(l)=arr(ir)
        arr(ir)=temp
        itemp=brr(l)
        brr(l)=brr(ir)
        brr(ir)=itemp
     endif
     if(arr(l+1).gt.arr(l))then
        temp=arr(l+1)
        arr(l+1)=arr(l)
        arr(l)=temp
        itemp=brr(l+1)
        brr(l+1)=brr(l)
        brr(l)=itemp
     endif
     i=l+1
     j=ir
     a=arr(l)
     b=brr(l)
3    continue
     i=i+1
     if(arr(i).lt.a)goto 3
4    continue
     j=j-1
     if(arr(j).gt.a)goto 4
     if(j.lt.i)goto 5
     temp=arr(i)
     arr(i)=arr(j)
     arr(j)=temp
     itemp=brr(i)
     brr(i)=brr(j)
     brr(j)=itemp
     goto 3
5    arr(l)=arr(j)
     arr(j)=a
     brr(l)=brr(j)
     brr(j)=b
     jstack=jstack+2
     if(jstack.gt.NSTACK) stop 'NSTACK too small in sort2i'
     if(ir-i+1.ge.j-l)then
        istack(jstack)=ir
        istack(jstack-1)=i
        ir=j-1
     else
        istack(jstack)=j-1
        istack(jstack-1)=l
        l=i
     endif
  endif
  goto 1
END SUBROUTINE sort2i

